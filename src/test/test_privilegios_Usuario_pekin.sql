
-- Debería funcionar
SELECT * FROM Usuario;
SELECT * FROM Usuario WHERE nombre LIKE 'Javier';

-- No debería funcionar al no tener permisos
INSERT INTO Usuario VALUES('dni', 'nombre', 'pass', 'apellidos', 24, 'F', 'correo@correo');
CREATE USER usuario IDENTIFIED BY 'techsuite.pekin';

SELECT * FROM mysql.user;
SHOW GRANTS FOR pekin;