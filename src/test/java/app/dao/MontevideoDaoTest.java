package app.dao;

import static org.mockito.Mockito.*;

import app.model.montevideo.*;
import java.math.BigInteger;
import java.sql.SQLException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MontevideoDaoTest extends GenericDaoTest {

  @Autowired
  private MontevideoDAO montevideoDAO;

  private Medicamento medicamento = new Medicamento(
    "Aspirina",
    "Contraindicacion1",
    "Posologia1",
    "Prospecto1",
    "Composicion1",
    "cima.aemps.es/aspirina",
    "2"
  );
  private Enfermedad enfermedad = new Enfermedad(
    "Migrañas",
    "Sintoma1",
    "Recomendacion1",
    "sescam.es",
    "2"
  );
  private ProductoNatural productoNatural = new ProductoNatural(
    "Té",
    "Contraindicacion1",
    "Posologia1",
    "Prospecto1",
    "Composicion1",
    "productosnaturales.com",
    "2"
  );

  @Test
  public void insertMedicamento_whenCalled_shouldExecuteRunner()
    throws SQLException {
    when(
      queryRunner.insert(
        eq(connection),
        anyString(),
        any(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString()
      )
    )
      .thenReturn(BigInteger.ONE); //idMedicamento = 1
    when(queryRunner.query(eq(connection), anyString(), any(), anyString()))
      .thenReturn(2); // idLenguaje = 2
    when(queryRunner.insert(eq(connection), anyString(), any(), anyString()))
      .thenReturn(BigInteger.valueOf(3)); // idURL = 3

    montevideoDAO.insertMedicamento(medicamento);

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq("Aspirina"),
        eq("Contraindicacion1"),
        eq("Posologia1"),
        eq("Prospecto1"),
        eq("Composicion1"),
        eq("Aspirina"),
        eq("Contraindicacion1"),
        eq("Posologia1"),
        eq("Prospecto1"),
        eq("Composicion1")
      );

    verify(queryRunner)
      .insert(eq(connection), anyString(), any(), eq(BigInteger.ONE), eq(2));

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq(BigInteger.ONE),
        eq(BigInteger.valueOf(3))
      );
  }

  @Test
  public void insertEnfermedad_whenCalled_shouldExecuteRunner()
    throws SQLException {
    when(
      queryRunner.insert(
        eq(connection),
        anyString(),
        any(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString()
      )
    )
      .thenReturn(BigInteger.ONE); //idEnfermedad = 1
    when(queryRunner.query(eq(connection), anyString(), any(), anyString()))
      .thenReturn(2); // idLenguaje = 2
    when(queryRunner.insert(eq(connection), anyString(), any(), anyString()))
      .thenReturn(BigInteger.valueOf(3)); // idURL = 3

    montevideoDAO.insertEnfermedad(enfermedad);

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq("Migrañas"),
        eq("Sintoma1"),
        eq("Recomendacion1"),
        eq("Migrañas"),
        eq("Sintoma1"),
        eq("Recomendacion1")
      );

    verify(queryRunner)
      .insert(eq(connection), anyString(), any(), eq(BigInteger.ONE), eq(2));

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq(BigInteger.ONE),
        eq(BigInteger.valueOf(3))
      );
  }

  @Test
  public void insertProductoNatural_whenCalled_shouldExecuteRunner()
    throws SQLException {
    when(
      queryRunner.insert(
        eq(connection),
        anyString(),
        any(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString(),
        anyString()
      )
    )
      .thenReturn(BigInteger.ONE); //idProductoNatural = 1
    when(queryRunner.query(eq(connection), anyString(), any(), anyString()))
      .thenReturn(2); // idLenguaje = 2
    when(queryRunner.insert(eq(connection), anyString(), any(), anyString()))
      .thenReturn(BigInteger.valueOf(3)); // idURL = 3

    montevideoDAO.insertProductoNatural(productoNatural);

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq("Té"),
        eq("Contraindicacion1"),
        eq("Posologia1"),
        eq("Prospecto1"),
        eq("Composicion1"),
        eq("Té"),
        eq("Contraindicacion1"),
        eq("Posologia1"),
        eq("Prospecto1"),
        eq("Composicion1")
      );

    verify(queryRunner)
      .insert(eq(connection), anyString(), any(), eq(BigInteger.ONE), eq(2));

    verify(queryRunner)
      .insert(
        eq(connection),
        anyString(),
        any(),
        eq(BigInteger.ONE),
        eq(BigInteger.valueOf(3))
      );
  }
}
