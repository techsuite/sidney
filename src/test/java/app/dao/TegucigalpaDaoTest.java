package app.dao;
import static org.mockito.Mockito.*;
import java.math.BigInteger;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import app.model.tegucigalpa.BloodTest;
import app.model.tegucigalpa.Employee;
import app.model.tegucigalpa.PCR;
import app.model.tegucigalpa.Patient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TegucigalpaDaoTest extends GenericDaoTest {
    @Autowired
    private TegucigalpaDAO tegucigalpaDAO;
    private Employee empleado = new Employee("4B", "Miguel", "Ceballos", "25/03/2000", "miguel@alumnos.upm.es", "Otro", "cebas325", "7798");
    private Patient paciente = new Patient("7D", "Antonio", "Ceballos", "24/09/1970", "aceballos@fi.upm.es", "Hombre");
    private BloodTest analisis = new BloodTest("7D", "4B", 1.0, 2.0, 3.0, 4.0, 5.0,"2020-01-01");
    private PCR pcr = new PCR("7D", "4B", 7.0,"2020-01-01");
    @Test
    public void insertEmpleado_whenCalled_shouldExecuteRunnerInsert() throws SQLException {
        tegucigalpaDAO.insertEmpleado(empleado);
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq("4B"),
          eq("Miguel"),
          eq("7798"),
          eq("Ceballos"),
          eq("25/03/2000"),
          eq("Otro"),
          eq("miguel@alumnos.upm.es")
        );
        verify(queryRunner, times(2)).insert(
          eq(connection),
          anyString(),
          any(),
          eq("4B")
        );
    }
      @Test 
      public void insertPaciente_whenCalled_shouldExecuteRunnerInsert() throws SQLException {
        tegucigalpaDAO.insertPaciente(paciente);
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq("7D"),
          eq("Antonio"),
          eq("Ceballos"),
          eq("24/09/1970"),
          eq("Hombre"),
          eq("aceballos@fi.upm.es")
        );
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq("7D")
        );
      }
      @Test
      public void insertAnalisisSangre_whenCalled_shouldExecuteRunnerInsert() throws SQLException {
        when(queryRunner.insert(eq(connection), anyString(), any(), anyString(), anyString(),anyString())).thenReturn(BigInteger.ONE);
        tegucigalpaDAO.insertAnalisisSangre(analisis);
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq("4B"),
          eq("7D"),
          eq("2020-01-01")
        );
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq(BigInteger.ONE),
          eq(1.0),
          eq(2.0),
          eq(3.0),
          eq(4.0),
          eq(5.0)
        );
      }
      @Test
      public void insertPCR_whenCalled_shouldExecuteRunnerInsert() throws SQLException {
        when(queryRunner.insert(eq(connection), anyString(), any(), anyString(), anyString(), anyString())).thenReturn(BigInteger.ONE);
        tegucigalpaDAO.insertPcr(pcr);
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq("4B"),
          eq("7D"),
          eq("2020-01-01")
        );
        verify(queryRunner).insert(
          eq(connection),
          anyString(),
          any(),
          eq(BigInteger.ONE),
          eq(7.0)
        );
      }
}
