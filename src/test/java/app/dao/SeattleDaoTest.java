package app.dao;

import app.model.seattle.*;

import static org.mockito.Mockito.*;

import java.math.BigInteger;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SeattleDaoTest extends GenericDaoTest {

  @Autowired
  private SeattleDAO seattleDAO;

  private Employee empleado = new Employee("1H", "Javier", "Médico", "2000-08-06", "aaa@222.com", "Hombre", "jg2000");
  private Patient paciente = new Patient("3H", "Javier", "Paciente", "2000-08-06", "aaa@222.com", "Hombre", "jg2000");
  private String[] enfermedades = new String[] {"enfermedad1"};
  private String[] medicamentos = new String[] {"medicamento1"};
  private String[] pruebasLab = new String[] {"PCR"};
  private String[] observaciones = new String[] {"observacion1"};
  private Consulta consulta = new Consulta(1, "1H", "3H", "2000-08-06", "2000-08-06", enfermedades, medicamentos, pruebasLab, observaciones);

  @Test
  public void insertEmpleado_whenCalled_shouldExecuteRunnerInsert() throws SQLException {

    seattleDAO.insertEmpleado(empleado);

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq("1H"),
      eq("Javier"),
      eq("jg2000"),
      eq("Médico"),
      eq("2000-08-06"),
      eq("Hombre"),
      eq("aaa@222.com")
      
    );

    verify(queryRunner, times(2)).insert(
      eq(connection),
      anyString(),
      any(),
      eq("1H")
    );
  }

  @Test
  public void insertPaciente_whenCalled_shouldExecuteRunnerInsert() throws SQLException {

    seattleDAO.insertPaciente(paciente);

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq("3H"),
      eq("Javier"),
      eq("Paciente"),
      eq("2000-08-06"),
      eq("Hombre"),
      eq("aaa@222.com")
      
    );

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq("3H")
    );
  }

  @Test
  public void insertConsulta_whenCalled_shouldExecuteRunnerInsert() throws SQLException {

    seattleDAO.insertConsulta(consulta);

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq("1H"),
      eq("3H"),
      eq("2000-08-06"),
      eq("observacion1\n")
    );
  }

  @Test
  public void insertConsultaEnfermedad_whenCalled_shouldExecuteRunnerQuery_and_RunnerInsert() throws SQLException {

    when(queryRunner.query(eq(connection), anyString(), any(), anyString())).thenReturn(1);

    seattleDAO.insertConsultaEnfermedad(consulta);

    verify(queryRunner).query(
      eq(connection),
      anyString(),
      any(),
      eq("enfermedad1")
    );

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq(1),
      eq("3H")
    );
  }

  @Test
  public void insertConsultaMedicamento_whenCalled_shouldExecuteRunnerQuery_and_RunnerInsert() throws SQLException {

    when(queryRunner.query(eq(connection), anyString(), any(), anyString())).thenReturn(1);

    seattleDAO.insertConsultaMedicamento(consulta);

    verify(queryRunner).query(
      eq(connection),
      anyString(),
      any(),
      eq("medicamento1")
    );

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq(1),
      eq("3H")
    );
  }

  @Test
  public void insertConsultaPruebaLab_whenCalled_shouldExecuteRunnerInsert() throws SQLException {

    when(queryRunner.insert(eq(connection), anyString(), any(), eq("3H"))).thenReturn(BigInteger.ONE);

    seattleDAO.insertConsultaPruebaLab(consulta);

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq("3H")
    );

    verify(queryRunner).insert(
      eq(connection),
      anyString(),
      any(),
      eq(BigInteger.ONE)
    );
  }

}
