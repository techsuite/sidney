package app.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import app.model.montevideo.*;
import app.model.seattle.*;
import app.model.tegucigalpa.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessingSubmoduleTest {

	@Autowired
    private ETLService servicioETL;
    
    // TEST MONTEVIDEO  ****************************************************************************
    // Todo correcto
    @Test
    public void testMontevideo1() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("Paracetamol", "No tomar padece enfermedades de hígado",
                "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                "Almidón de maíz pregelatinizado", "cima.aemps.es/paracetamol", "english") };
        Enfermedad[] enfermedades = { new Enfermedad("Migrañas", "Fiebre, dolores de cabeza",
                "Tomar medicamento dos veces al día", "sescam.es", "spanish") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Jengibre", "Nada", "Jengibre",
                "Tomar de una a tres veces al dia con agua", "Agua y jengibre", "productosnaturales.com", "spanish") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);

       assertTrue(res);
    }

    // Todo correcto 2
    @Test
    public void testMontevideo2() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("Paracetamol", "No tomar padece enfermedades de hígado",
               "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                "Almidón de maíz pregelatinizado", "cima.aemps.es/paracetamol", "english"), new Medicamento("Ibuprofeno", "No tomar padece enfermedades de hígado",
                "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                " celulosa microcristalina", "cima.aemps.es/ibuprofeno", "english") };
        Enfermedad[] enfermedades = { new Enfermedad("Migrañas", "Fiebre, dolores de cabeza",
                "Tomar medicamento dos veces al día", "sescam.es", "spanish") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Jengibre", "Nada", "Jengibre",
                "Tomar de una a tres veces al dia con agua", "Agua y jengibre", "productosnaturales.com", "spanish") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);
    
		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);
    
       assertTrue(res);
        }

    // Todo incorrecto
    @Test
    public void testMontevideoMal() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("", "","", "","", "", "") };
        Enfermedad[] enfermedades = { new Enfermedad("", "","", "", "") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("", "", "","", "", "", "") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);

        assertFalse(res);
    }

    // Contraseña incorrecta
    @Test
    public void testMontevideo3() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("Paracetamol", "No tomar padece enfermedades de hígado",
                "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                "Almidón de maíz pregelatinizado", "cima.aemps.es/paracetamol", "english") };
        Enfermedad[] enfermedades = { new Enfermedad("Migrañas", "Fiebre, dolores de cabeza",
                "Tomar medicamento dos veces al día", "sescam.es", "spanish") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Jengibre", "Nada", "Jengibre",
                "Tomar de una a tres veces al dia con agua", "Agua y jengibre", "productosnaturales.com", "spanish") };
        DataMontevideo mData = new DataMontevideo("contrasenia", medicamentos, enfermedades, productosNatrls);

		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);

        assertFalse(res);
    }

    // Falta campo nombre medicamento
    @Test
    public void testMontevideo4() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("", "No tomar con molestias gástricas",
                "1 comprimido cada 6-4 horas", "El principio activo de este medicamento es...",
                "Ácido acetilsalicílico(500mg)", "cima.aemps.es/aspirina", "english") };
        Enfermedad[] enfermedades = { new Enfermedad("Migrañas", "Dolor de cabeza constante, naúseas y mareos, vómitos",
                "Tomar medicamento cada 6 horas, evitar largas exposiciones a pantallas", "sescam.es", "english") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Té", "Nada", "Planta de té",
                "Despúes de la comida o la cena", "Agua y bolsa de té", "productosnaturales.com", "spanish") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);

        assertFalse(res);
    }

    // Idioma incorrecto
    @Test
    public void testMontevideo5() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("Aspirina", "No tomar con molestias gástricas",
                "1 comprimido cada 6-4 horas", "El principio activo de este medicamento es...",
                "Ácido acetilsalicílico(500mg)", "cima.aemps.es/aspirina", "english") };
        Enfermedad[] enfermedades = { new Enfermedad("Migrañas", "Dolor de cabeza constante, naúseas y mareos, vómitos",
                "Tomar medicamento cada 6 horas, evitar largas exposiciones a pantallas", "", "spanish") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Té", "Nada", "Planta de té",
                "Despúes de la comida o la cena", "Agua y bolsa de té", "productosnaturales.com", "espaniol") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);

        assertFalse(res);
    }

    // Campos correctos e incorrectos
    @Test
    public void testMontevideo6() {
        boolean res;
        Medicamento[] medicamentos = { new Medicamento("Paracetamol", "No tomar padece enfermedades de hígado",
               "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                "Almidón de maíz pregelatinizado", "cima.aemps.es/paracetamol", "english"), new Medicamento("Ibuprofeno", "No tomar padece enfermedades de hígado",
                "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                " celulosa microcristalina", "cima.aemps.es/ibuprofeno", "english"), new Medicamento("", "","", "","", "", "")};
        Enfermedad[] enfermedades = { new Enfermedad("", "","", "", ""), new Enfermedad("Migrañas", "Fiebre, dolores de cabeza",
                "Tomar medicamento dos veces al día", "sescam.es", "spanish") };
        ProductoNatural[] productosNatrls = { new ProductoNatural("Jengibre", "Nada", "Jengibre",
                "Tomar de una a tres veces al dia con agua", "Agua y jengibre", "productosnaturales.com", "spanish"),new ProductoNatural("", "", "","", "", "", "") };
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);
    
		servicioETL.setDebug(true);
        res = servicioETL.proccessMontevideo(mData);
    
        assertFalse(res);  // devuelve false si el dato enviado esta completo y bien, como en este caso
    }

        // Con huecos
        @Test
        public void testMontevideo7() {
                boolean res;
                Medicamento[] medicamentos = { new Medicamento(" ", "No tomar padece enfermedades de hígado",
                        "1 comprimido cada 8 horas", "El principio activo de este medicamento es...",
                        "Almidón de maíz pregelatinizado", "cima.aemps.es/paracetamol", "english") };
                Enfermedad[] enfermedades = { new Enfermedad("   ", "  ",
                        "Tomar medicamento dos veces al día", "sescam.es", "spanish") };
                ProductoNatural[] productosNatrls = { new ProductoNatural("Jengibre", "Nada", "Jengibre",
                        "Tomar de una a tres veces al dia con agua", "Agua y jengibre", "productosnaturales.com", "spanish") };
                DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

				servicioETL.setDebug(true);
                res = servicioETL.proccessMontevideo(mData);

                assertFalse(res);
        }

    // TEST TEGUCIGALPA  ****************************************************************************
    // Todo correcto
    @Test
    public void testTegucigalpa1() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3,"2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

       assertTrue(res);
    }

    // Todo incorrecto
    @Test
    public void testTegucigalpa2() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("", "", "", "","", "", "", "") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("", "", "", "","", "") };
        BloodTest bloodTests[] = { new BloodTest("", "", 0.0, 0.0, 0.0, 0.0, 0.0,"2020-01-01") };
        PCR pcrs[] = { new PCR("", "", 0.0,"2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

        assertFalse(res);
    }

    // Falta campo dni empleado
    @Test
    public void testTegucigalpa3() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3,"2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

        assertFalse(res);
    }

    // Falta campo dni paciente
    @Test
    public void testTegucigalpa4() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3,"2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

        assertFalse(res);
    }

    // Contraseña incorrecta
    @Test
    public void testTegucigalpa5() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3,"2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

        assertFalse(res);
    }

    @Test
    // Campos correctos e incorrectos
    public void testTegucigalpa6() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354"), new app.model.tegucigalpa.Employee("", "", "", "","", "", "", "")};
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino"), new app.model.tegucigalpa.Patient("", "", "", "","", "") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01"), new BloodTest("", "", 0.0, 0.0, 0.0, 0.0, 0.0,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3, "2020-01-01"), new PCR("", "", 0.0, "2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

        assertFalse(res);
    }

    @Test
    // Empleado incorrecto
    public void testTegucigalpa7() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "", "Gómez García", "2000-01-03", "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "", "2000-01-03", "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "", "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03", "", "femenino", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03", "belen.gomez@alumnos.upm.es", "f", "belengg", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03", "belen.gomez@alumnos.upm.es", "femenino", "", "21354"),
                new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03", "belen.gomez@alumnos.upm.es", "femenino", "belengg", "") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9,"2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3, "2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

       assertFalse(res);
    }

    @Test
    // Pacientes incorrectos
    public void testTegucigalpa8() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino"),
                new app.model.tegucigalpa.Patient("", "Antonio", "Cano Meneses", "2000-01-03", "antonio.cano@alumnos.upm.es", "masculino"),
                new app.model.tegucigalpa.Patient("83625783J", "", "Cano Meneses", "2000-01-03", "antonio.cano@alumnos.upm.es", "masculino"),
                new app.model.tegucigalpa.Patient("83625783J", "Antonio", "", "2000-01-03", "antonio.cano@alumnos.upm.es", "masculino"),
                new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "", "antonio.cano@alumnos.upm.es", "masculino"),
                new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03", "", "masculino"),
                new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03", "antonio.cano@alumnos.upm.es", "m") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3, "2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

       assertFalse(res);
    }

    @Test
    // BloodTest y PCR incorrectos
    public void testTegucigalpa9() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("025531624A", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "Antonio", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "50338767A", -10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "50338767A", 10.0, -8.3, 4.1, 5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "50338767A", 10.0, 8.3, -4.1, 5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, -5.5, 7.9, "2020-01-01"),
                new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, -7.9, "2020-01-01") };
        PCR pcrs[] = { new PCR("", "50338766Z", 6.3, "2020-01-01"), new PCR("83625783J", "", 6.3, "2020-01-01"), new PCR("83625783J", "50338766Z", -6.3, "2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

       assertFalse(res);
    }

    // Con huecos
    @Test
    public void testTegucigalpa10() {
        boolean res;

        app.model.tegucigalpa.Employee employees[] = { new app.model.tegucigalpa.Employee("   ", "Belén", "Gómez García", "2000-01-03",
                "belen.gomez@alumnos.upm.es", "femenino", "belengg", "21354") };
        app.model.tegucigalpa.Patient patients[] = { new app.model.tegucigalpa.Patient("83625783J", "   ", "Cano Meneses", "2000-01-03",
                "antonio.cano@alumnos.upm.es", "masculino") };
        BloodTest bloodTests[] = { new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01") };
        PCR pcrs[] = { new PCR("83625783J", "50338766Z", 6.3, "2020-01-01") };
        DataTegucigalpa tData = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

		servicioETL.setDebug(true);
        res = servicioETL.proccessTegucigalpa(tData);

       assertFalse(res);
    }


    // TEST SEATTLE  ****************************************************************************
    // Todo correcto
    @Test
    public void testSeattle1() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

       assertTrue(res);
    }

    // Todo incorrecto
    @Test
    public void testSeattle2() {
        boolean res;

        app.model.seattle.Employee[] employees = {new app.model.seattle.Employee("", "", "", "", "", "",  "") };
        app.model.seattle.Patient[] patients = {new app.model.seattle.Patient("", "", "", "", "", "", "") };
        String[] enfermedades = new String[] { "" };
        String[] medicamentos = new String[] { "" };
        String[] pruebasLab = new String[] { "",""};
        String[] observaciones = new String[] { "" };
        Consulta[] consultas = { new Consulta(0, "", "", "", "", enfermedades, medicamentos,pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Falta campo DNI en empleado
    @Test
    public void testSeattle3() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Falta campo DNI en paciente
    @Test
    public void testSeattle4() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };
        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Falta campo DNI en consulta
    @Test
    public void testSeattle5() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "", "", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };
        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Contrasenia incorrecta
    @Test
    public void testSeattle6() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };
        DataSeattle sData = new DataSeattle("as#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Campos correctos e incorrectos
    @Test
    public void testSeattle7() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411"), new app.model.seattle.Employee("", "", "", "", "", "",  "") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", ""), new app.model.seattle.Patient("", "", "", "", "", "", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    @Test
    // Empleados incorrectos
    public void testSeattle8() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411"),
                new app.model.seattle.Employee("5532125A", "Belén", "", "2000-01-03", "belen@gmail.com", "femenino",  "45411"),
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "12-58-6", "belen@gmail.com", "femenino",  "45411"),
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "", "femenino",  "45411"),
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "f",  "45411"),
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "")
         };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Pacientes incorrectos
    @Test
    public void testSeattle9() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", ""),
                new app.model.seattle.Patient("421321325F", "", "González", "2000-01-03", "javier@gmail.com", "masculino", ""),
                new app.model.seattle.Patient("421321325F", "Javier", "", "2000-01-03", "javier@gmail.com", "masculino", ""),
                new app.model.seattle.Patient("421321325F", "Javier", "González", "", "javier@gmail.com", "masculino", ""),
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "", "masculino", ""),
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Consultas incorrectas
    @Test
    public void testSeattle10() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("5532125A", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "Javier", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "", "421321325F", "2000-01-03", "2000-01-03", enfermedades, medicamentos, pruebasLab, observaciones),
                new Consulta(1, "5532125A", "", "2000-01-03", "2000-01-03", enfermedades, medicamentos, pruebasLab, observaciones),
                new Consulta(1, "5532125A", "421321325F", "", "2000-01-03", enfermedades, medicamentos, pruebasLab, observaciones), 
                new Consulta(1, "5532125A", "421321325F", "2000-01-03", "", enfermedades, medicamentos, pruebasLab, observaciones)};

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

        assertFalse(res);
    }

    // Espacios en campos
    @Test
    public void testSeattle11() {
        boolean res;

        app.model.seattle.Employee[] employees = {
                new app.model.seattle.Employee("   ", "Belén", "Gómez García", "2000-01-03", "belen@gmail.com", "femenino",  "45411") };
        app.model.seattle.Patient[] patients = {
                new app.model.seattle.Patient("421321325F", "   ", "González", "2000-01-03", "javier@gmail.com", "masculino", "") };
        String[] enfermedades = new String[] { "covid" };
        String[] medicamentos = new String[] { "paracetamol" };
        String[] pruebasLab = new String[] { "PCR","analisis"};
        String[] observaciones = new String[] { "fiebre" };
        Consulta[] consultas = { new Consulta(1, "5532125A", "  ", "2000-01-03", "2000-01-03", enfermedades, medicamentos,
                pruebasLab, observaciones) };

        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);

		servicioETL.setDebug(true);
        res = servicioETL.proccessSeattle(sData);

       assertFalse(res);
    }
}
