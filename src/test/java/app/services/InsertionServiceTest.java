package app.services;

import app.dao.MontevideoDAO;
import app.dao.SeattleDAO;
import app.dao.TegucigalpaDAO;
import app.model.seattle.*;
import app.model.tegucigalpa.*;
import app.model.montevideo.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;


import java.sql.SQLException;

import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class InsertionServiceTest {

    @Autowired
    private InsertionService service;

    @MockBean
    SeattleDAO seattleDAO;

    @MockBean
    TegucigalpaDAO tegucigalpaDAO;

    @MockBean
    MontevideoDAO montevideoDAO;

    @Test
    public void insertSeattleTest() throws SQLException{

        app.model.seattle.Employee[] empleados = {new app.model.seattle.Employee("1H", "Javier", "Médico", "2000-08-06", "aaa@222.com", "Hombre", "jg2000")};
        app.model.seattle.Patient[] pacientes = {new app.model.seattle.Patient("3H", "Javier", "Paciente", "2000-08-06", "aaa@222.com", "Hombre", "jg2000")};
        String[] enfermedades = new String[] {"enfermedad1"};
        String[] medicamentos = new String[] {"medicamento1"};
        String[] pruebasLab = new String[] {"PCR"};
        String[] observaciones = new String[] {"observacion1"};
        Consulta[] consultas = {new Consulta(1, "1H", "3H", "2000-08-06", "2000-08-06", enfermedades, medicamentos, pruebasLab, observaciones)};
        DataSeattle sData = new DataSeattle("key", empleados, pacientes, consultas);

        service.insertSeattle(sData);

        verify(seattleDAO).insertEmpleado(empleados[0]);
        verify(seattleDAO).insertPaciente(pacientes[0]);
        verify(seattleDAO).insertConsulta(consultas[0]);
        verify(seattleDAO).insertConsultaMedicamento(consultas[0]);
        verify(seattleDAO).insertConsultaEnfermedad(consultas[0]);
        verify(seattleDAO).insertConsultaPruebaLab(consultas[0]);

    }

    @Test
    public void insertionTegucigalpaTest() throws SQLException {
        app.model.tegucigalpa.Employee[] empleados = {new app.model.tegucigalpa.Employee("5A", "Miguel", "Ceballos Gomez", "2000-03-25", "miguel.ceballos@alumnos.upm.es", "Hombre", "cebas325", "6698")};
        app.model.tegucigalpa.Patient pacientes[] = {new app.model.tegucigalpa.Patient("8J", "Antonio", "Cano Meneses", "2000-01-01", "antonio.cano@alumnos.upm.es", "Mujer")};
        BloodTest[] bloodTests = {new BloodTest("8J", "5A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01")};
        PCR[] pcrs = {new PCR("8J", "5A", 6.3, "2020-01-01")};
        DataTegucigalpa tData  = new DataTegucigalpa("key", empleados, pacientes, bloodTests, pcrs);

        service.insertTegucigalpa(tData);

        verify(tegucigalpaDAO).insertEmpleado(empleados[0]);
        verify(tegucigalpaDAO).insertPaciente(pacientes[0]);
        verify(tegucigalpaDAO).insertAnalisisSangre(bloodTests[0]);
        verify(tegucigalpaDAO).insertPcr(pcrs[0]);
    }

    @Test
    public void insertionMontevideoTest() throws SQLException {
        Medicamento[] medicamentos = {new Medicamento("Aspirina", "No tomar con molestias gástricas", "1 comprimido cada 6-4 horas", "El principio activo de este medicamento es...", "Ácido acetilsalicílico(500mg)", "cima.aemps.es/aspirina", "Spanish")}; 
        Enfermedad[] enfermedades = {new Enfermedad("Migrañas", "Dolor de cabeza constante, naúseas y mareos, vómitos", "Tomar medicamento cada 6 horas, evitar largas exposiciones a pantallas", "sescam.es", "Spanish")};
        ProductoNatural[] productosNatrls = {new ProductoNatural("Té", "Nada", "Planta de té", "Despúes de la comida o la cena", "Agua y bolsa de té", "productosnaturales.com", "Spanish")};
        DataMontevideo mData = new DataMontevideo("key", medicamentos, enfermedades, productosNatrls);
        
        service.insertMontevideo(mData);
        
        verify(montevideoDAO).insertEnfermedad(enfermedades[0]);
        verify(montevideoDAO).insertMedicamento(medicamentos[0]);
        verify(montevideoDAO).insertProductoNatural(productosNatrls[0]);
    }
}