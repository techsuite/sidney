package app.controller;

import static org.junit.Assert.assertEquals;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.http.MediaType;

import app.model.seattle.DataSeattle;
import app.model.seattle.Employee;
import app.model.seattle.Patient;
import app.services.ETLService;
import app.model.seattle.Consulta;

@RunWith(SpringRunner.class)
@WebMvcTest(ETLController.class)

public class SeattleControlerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ETLService servicioETL ;

    @Test
    public void insertDataSeattle() throws Exception {

        Employee[] employees = {new Employee("1H", "Javier", "Médico", "08/06/2000", "aaa@222.com", "Varon", "jg2000")};
        Patient[] patients = {new Patient("3H", "Javier", "Paciente", "08/06/2000", "aaa@222.com", "Varon", "jg2000")};
        String[] enfermedades = new String[] {"enfermedad1"};
        String[] medicamentos = new String[] {"medicamento1"};
        String[] pruebasLab = new String[] {"pruebaLab1"};
        String[] observaciones = new String[] {"observacion1"};
        Consulta[] consultas = {new Consulta(1, "1H", "Javier", "09/06/2000", "09/06/2000", enfermedades, medicamentos, pruebasLab, observaciones)};
        DataSeattle sData = new DataSeattle("W6YRf#*ZLsZ1", employees, patients, consultas);
        
        mockMvc.perform(
            MockMvcRequestBuilders
              .post("/getDataSeattle")
              .content(new JSONObject(sData).toString())
              .contentType(MediaType.APPLICATION_JSON)
          );
        assertEquals(true, true);

    }
    
}
