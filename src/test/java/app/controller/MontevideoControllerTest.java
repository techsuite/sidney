package app.controller;

import static org.junit.Assert.assertEquals;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import app.model.montevideo.*;
import app.services.ETLService;



@RunWith(SpringRunner.class)
@WebMvcTest(ETLController.class)
public class MontevideoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ETLService servicioETL ;

    @Test
    public void testDataMontevideo() throws Exception {

        Medicamento[] medicamentos = {new Medicamento("Aspirina", "No tomar con molestias gástricas", "1 comprimido cada 6-4 horas", "El principio activo de este medicamento es...", "Ácido acetilsalicílico(500mg)", "cima.aemps.es/aspirina", "espanol")};
        Enfermedad[] enfermedades = {new Enfermedad("Migrañas", "Dolor de cabeza constante, naúseas y mareos, vómitos", "Tomar medicamento cada 6 horas, evitar largas exposiciones a pantallas", "sescam.es", "espanol")};
        ProductoNatural[] productosNatrls = {new ProductoNatural("Té", "Nada", "Planta de té", "Despúes de la comida o la cena", "Agua y bolsa de té", "productosnaturales.com", "espanol")};
        DataMontevideo mData = new DataMontevideo("Fl9z%oJu#pLb", medicamentos, enfermedades, productosNatrls);

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/getDataMontevideo")
                .content(new JSONObject(mData).toString())
                .contentType(MediaType.APPLICATION_JSON)
        );
        assertEquals(true, true);

    }
    
}
