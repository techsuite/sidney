package app.controller;

import static org.junit.Assert.assertEquals;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import app.model.tegucigalpa.BloodTest;
import app.model.tegucigalpa.DataTegucigalpa;
import app.model.tegucigalpa.Employee;
import app.model.tegucigalpa.PCR;
import app.model.tegucigalpa.Patient;
import app.services.ETLService;

@RunWith(SpringRunner.class)
@WebMvcTest(ETLController.class)

public class TegucigalpaControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ETLService servicioETL ;

    @Test
    public void test1() throws Exception {
        Employee employee = new Employee("50338766Z", "Miguel", "Ceballos Gomez", "25/03/2000", "miguel.ceballos@alumnos.upm.es", "masculino", "cebas325", "6698");
        Employee employees[] = {employee}; 
        Patient patient = new Patient("83625783J", "Antonio", "Cano Meneses", "01/01/2000", "antonio.cano@alumnos.upm.es", "masculino");
        Patient patients[] = {patient};
        BloodTest bloodTest = new BloodTest("83625783J", "50338767A", 10.0, 8.3, 4.1, 5.5, 7.9, "2020-01-01");
        BloodTest bloodTests[] = {bloodTest};
        PCR pcr = new PCR("83625783J", "50338766Z", 6.3,"2020-01-01");
        PCR pcrs[] = {pcr};
        DataTegucigalpa tData  = new DataTegucigalpa("qnxCE%MIA%m9", employees, patients, bloodTests, pcrs);

        mockMvc.perform(
            MockMvcRequestBuilders
              .post("/getDataTegucigalpa")
              .content(new JSONObject(tData).toString())
              .contentType(MediaType.APPLICATION_JSON)
        );

        assertEquals(true, true);
    }

}
