package app.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import app.model.testPost.NumberClass;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(HealthController.class)
public class HealthControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void alive_whenCallingWithNoParameters_shouldReturnCorrectStatus()
    throws Exception {
    mockMvc
      .perform(get("/status"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(1)))
      .andExpect(jsonPath("$.status", is("Stayin Alive!")));
  }
  @Test
  public void NotCorrect()
    throws Exception {
    mockMvc
      .perform(get("/status/1"))
      .andExpect(status().isNotFound());
  }
  public static String asJsonString(final Object obj) {
    try {
        return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
}

  @Test
    public void insertDataFull2round() throws Exception {
      mockMvc.perform(MockMvcRequestBuilders.post("/status").content(asJsonString(new NumberClass("seattle", 69.0)) ));
      
      assertEquals(true,true);
      
  }
}
