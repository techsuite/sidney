package app.dataStructures;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;


/**
 * Test for class Pair, check if everything works as intended.
 */
public class PairTest {
    @Test
    public void testConstructorNulled() throws Exception {
        Pair<Integer,Integer> par = new Pair(null,null);
        assertEquals(par.first, null);
        assertEquals(par.second, null);
    }
    @Test
    public void testConstructorUnNulled() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        assertEquals(par.first, 6);
        assertEquals(par.second, 9);
    }
    @Test
    public void testEqualsTrue() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        Pair<Integer,Integer> par2 = new Pair(6,9);
       assertEquals(par.equals(par2),true);
    }
    public void testEqualsFalse() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        Pair<Integer,Integer> par2 = new Pair(7,9);
       assertEquals(par.equals(par2),false);
    }
    public void testEqualsFalseSecond() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        Pair<Integer,Integer> par2 = new Pair(6,29);
       assertEquals(par.equals(par2),false);
    }
    public void testEqualsNulled() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        Pair<Integer,Integer> par2 = new Pair(null,null);
       assertEquals(par.equals(par2),false);
    }
    @Test
    public void test2String() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        assertEquals(par.toString(), "(6, 9)");
        
    }
    @Test
    public void hashCodeTest() throws Exception {
        Pair<Integer,Integer> par = new Pair(6,9);
        assertEquals(par.hashCode(),31 * par.first.hashCode() + par.second.hashCode());
    }
}
