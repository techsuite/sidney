CREATE USER 'sydney' IDENTIFIED BY 'techsuite.sydney';

GRANT SELECT ON core.Internal TO 'sydney';
GRANT UPDATE ON core.Internal TO 'sydney';
GRANT DELETE ON core.Internal TO 'sydney';
GRANT INSERT ON core.Internal TO 'sydney';

SELECT * FROM mysql.user;
SHOW GRANTS FOR sydney;
