USE core;

CREATE TABLE Usuario(
DNI varchar(20),
nombre varchar(60),
contrasena varchar(60),
apellidos varchar(60),
edad int,
genero varchar(10),
correo_electronico varchar(60),
PRIMARY KEY (DNI)
);

CREATE INDEX correo_electronico ON Usuario (correo_electronico);
