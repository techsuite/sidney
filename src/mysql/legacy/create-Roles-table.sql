USE core;

CREATE TABLE Roles(
    rol varchar(255),
    PRIMARY KEY (rol)
);

INSERT INTO Roles (rol)
VALUES ('Doctor');

INSERT INTO Roles (rol)
VALUES ('Trabajador de laboratorio');

INSERT INTO Roles (rol)
VALUES ('Paciente');

INSERT INTO Roles (rol)
VALUES ('Impostor');