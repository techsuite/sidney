CREATE TABLE `Usuario` (
	`ID` INT(10) NOT NULL AUTO_INCREMENT,
	`DNI` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nombre` VARCHAR(60) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`contrasena` VARCHAR(60) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`apellidos` VARCHAR(60) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`edad` INT(10) NULL DEFAULT NULL,
	`genero` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`correo_electronico` VARCHAR(60) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`ID`, `DNI`) USING BTREE,
	INDEX `correo_electronico` (`correo_electronico`) USING BTREE
);
ALTER TABLE Usuario CHANGE COLUMN nombre nombre VARCHAR(60) NULL COLLATE 'utf8mb4_0900_as_cs';