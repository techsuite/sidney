
-- Creación del usuario 'pekin' con contraseña 'techsuite.pekin'
CREATE User pekin IDENTIFIED BY 'techsuite.pekin';
-- Otorgar privilegios al usuario
GRANT ALL PRIVILEGES ON core.* TO pekin WITH GRANT OPTION;
FLUSH PRIVILEGES;

-- Mostrar usuarios y privilegios
SELECT * FROM mysql.user;
SHOW GRANTS FOR pekin;


-- Consultas sobre la BBDD a ejecutar desde el usuario 'pekin' para comprobar funcionamiento
INSERT INTO User VALUES (3, 'Belén', '2700bgg');
SELECT * FROM User;
DELETE FROM `core`.`User` WHERE `id`=3;



