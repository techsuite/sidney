USE core;
CREATE TABLE User (
	id int 	PRIMARY KEY UNIQUE NOT NULL,
    name VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);

INSERT INTO User
VALUES (1, "Miguel", "3007mcg"),
(2, "Javier", "2804jdg");