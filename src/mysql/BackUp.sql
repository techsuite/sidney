-- --------------------------------------------------------
-- Host:                         juliocastrodev.duckdns.org
-- Versión del servidor:         8.0.23 - MySQL Community Server - GPL
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla core.AnalisisSangre
DROP TABLE IF EXISTS `AnalisisSangre`;
CREATE TABLE IF NOT EXISTS `AnalisisSangre` (
  `idPrueba` int NOT NULL,
  `Electrolitos` double DEFAULT NULL,
  `Glucosa` double DEFAULT NULL,
  `Colesterol` double DEFAULT NULL,
  `Trigliceridos` double DEFAULT NULL,
  `Bilirrubina` double DEFAULT NULL,
  PRIMARY KEY (`idPrueba`),
  KEY `fk_AnalisisSangre_Prueba1_idx` (`idPrueba`),
  CONSTRAINT `fk_AnalisisSangre_Prueba1` FOREIGN KEY (`idPrueba`) REFERENCES `Prueba` (`idPrueba`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Consulta
DROP TABLE IF EXISTS `Consulta`;
CREATE TABLE IF NOT EXISTS `Consulta` (
  `idConsulta` int NOT NULL AUTO_INCREMENT,
  `PacienteDNI` varchar(10) NOT NULL,
  `FechaRealizacion` datetime DEFAULT NULL,
  `Observaciones` VARCHAR(500),
  `DoctorDNI` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idConsulta`),
  KEY `fk_Consulta_Paciente1_idx` (`PacienteDNI`),
  KEY `fk_Consulta_Doctor1_idx` (`DoctorDNI`) USING BTREE,
  CONSTRAINT `fk_Consulta_Doctor1` FOREIGN KEY (`DoctorDNI`) REFERENCES `Doctor` (`DoctorDNI`),
  CONSTRAINT `fk_Consulta_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`),
  UNIQUE(PacienteDNI, DoctorDNI, Observaciones)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Doctor
DROP TABLE IF EXISTS `Doctor`;
CREATE TABLE IF NOT EXISTS `Doctor` (
  `DoctorDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`DoctorDNI`),
  CONSTRAINT `fk_Doctor_Usuario1` FOREIGN KEY (`DoctorDNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Enfermedad
DROP TABLE IF EXISTS `Enfermedad`;
CREATE TABLE IF NOT EXISTS `Enfermedad` (
  `idEnfermedad` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Sintomas` mediumtext,
  `Recomendaciones` mediumtext,
  PRIMARY KEY (`idEnfermedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Enfermedad-Lenguaje
DROP TABLE IF EXISTS `Enfermedad-Lenguaje`;
CREATE TABLE IF NOT EXISTS `Enfermedad-Lenguaje` (
  `idEnfermedad` int NOT NULL,
  `idLenguaje` int NOT NULL,
  PRIMARY KEY (`idEnfermedad`,`idLenguaje`),
  KEY `fk_Enfermedad_has_Lenguaje_Lenguaje1_idx` (`idLenguaje`),
  KEY `fk_Enfermedad_has_Lenguaje_Enfermedad1_idx` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_Lenguaje_Enfermedad1` FOREIGN KEY (`idEnfermedad`) REFERENCES `Enfermedad` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_Lenguaje_Lenguaje1` FOREIGN KEY (`idLenguaje`) REFERENCES `Lenguaje` (`idLenguaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Enfermedad-Sintomas
DROP TABLE IF EXISTS `Enfermedad-Sintomas`;
CREATE TABLE IF NOT EXISTS `Enfermedad-Sintomas` (
  `idSintomas` int NOT NULL,
  `idEnfermedad` int NOT NULL,
  PRIMARY KEY (`idSintomas`,`idEnfermedad`),
  KEY `fk_Sintomas_has_Enfermedad_Enfermedad1_idx` (`idEnfermedad`),
  KEY `fk_Sintomas_has_Enfermedad_Sintomas1_idx` (`idSintomas`),
  CONSTRAINT `fk_Sintomas_has_Enfermedad_Enfermedad1` FOREIGN KEY (`idEnfermedad`) REFERENCES `Enfermedad` (`idEnfermedad`),
  CONSTRAINT `fk_Sintomas_has_Enfermedad_Sintomas1` FOREIGN KEY (`idSintomas`) REFERENCES `Sintomas` (`idSintomas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Enfermedad-URL
DROP TABLE IF EXISTS `Enfermedad-URL`;
CREATE TABLE IF NOT EXISTS `Enfermedad-URL` (
  `idEnfermedad` int NOT NULL,
  `idURL` int NOT NULL,
  PRIMARY KEY (`idEnfermedad`,`idURL`),
  KEY `fk_Enfermedad_has_URL_URL1_idx` (`idURL`),
  KEY `fk_Enfermedad_has_URL_Enfermedad1_idx` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_URL_Enfermedad1` FOREIGN KEY (`idEnfermedad`) REFERENCES `Enfermedad` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_URL_URL1` FOREIGN KEY (`idURL`) REFERENCES `URL` (`idURL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Impostor
DROP TABLE IF EXISTS `Impostor`;
CREATE TABLE IF NOT EXISTS `Impostor` (
  `ImpostorDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`ImpostorDNI`),
  CONSTRAINT `FK_Impostor_Usuario` FOREIGN KEY (`ImpostorDNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Internal
DROP TABLE IF EXISTS `Internal`;
CREATE TABLE IF NOT EXISTS `Internal` (
  `senderID` varchar(60) DEFAULT NULL,
  `number` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Lenguaje
DROP TABLE IF EXISTS `Lenguaje`;
CREATE TABLE IF NOT EXISTS `Lenguaje` (
  `idLenguaje` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `ISO639-1` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idLenguaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Medicamento
DROP TABLE IF EXISTS `Medicamento`;
CREATE TABLE IF NOT EXISTS `Medicamento` (
  `idMedicamento` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Contraindicaciones` mediumtext,
  `Posologia` mediumtext,
  `Prospecto` mediumtext,
  `Composicion` mediumtext,
  PRIMARY KEY (`idMedicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Medicamento-Lenguaje
DROP TABLE IF EXISTS `Medicamento-Lenguaje`;
CREATE TABLE IF NOT EXISTS `Medicamento-Lenguaje` (
  `idMedicamento` int NOT NULL,
  `idLenguaje` int NOT NULL,
  PRIMARY KEY (`idMedicamento`,`idLenguaje`),
  KEY `fk_Medicamento_has_Lenguaje_Lenguaje1_idx` (`idLenguaje`),
  KEY `fk_Medicamento_has_Lenguaje_Medicamento1_idx` (`idMedicamento`),
  CONSTRAINT `fk_Medicamento_has_Lenguaje_Lenguaje1` FOREIGN KEY (`idLenguaje`) REFERENCES `Lenguaje` (`idLenguaje`),
  CONSTRAINT `fk_Medicamento_has_Lenguaje_Medicamento1` FOREIGN KEY (`idMedicamento`) REFERENCES `Medicamento` (`idMedicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Medicamento-URL
DROP TABLE IF EXISTS `Medicamento-URL`;
CREATE TABLE IF NOT EXISTS `Medicamento-URL` (
  `idMedicamento` int NOT NULL,
  `idURL` int NOT NULL,
  PRIMARY KEY (`idMedicamento`,`idURL`),
  KEY `fk_Medicamento_has_URL_URL1_idx` (`idURL`),
  KEY `fk_Medicamento_has_URL_Medicamento1_idx` (`idMedicamento`),
  CONSTRAINT `fk_Medicamento_has_URL_Medicamento1` FOREIGN KEY (`idMedicamento`) REFERENCES `Medicamento` (`idMedicamento`),
  CONSTRAINT `fk_Medicamento_has_URL_URL1` FOREIGN KEY (`idURL`) REFERENCES `URL` (`idURL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.OTP
DROP TABLE IF EXISTS `OTP`;
CREATE TABLE IF NOT EXISTS `OTP` (
  `idOTP` int NOT NULL AUTO_INCREMENT,
  `PacienteDNI` varchar(10) NOT NULL,
  `Valor` int DEFAULT NULL,
  `Expiracion` datetime DEFAULT NULL,
  `Tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idOTP`),
  KEY `fk_OTP_Paciente1_idx` (`PacienteDNI`),
  CONSTRAINT `fk_OTP_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Paciente
DROP TABLE IF EXISTS `Paciente`;
CREATE TABLE IF NOT EXISTS `Paciente` (
  `PacienteDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`PacienteDNI`),
  KEY `fk_Paciente_Usuario1_idx` (`PacienteDNI`),
  CONSTRAINT `fk_Paciente_Usuario1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Paciente-Enfermedad
DROP TABLE IF EXISTS `Paciente-Enfermedad`;
CREATE TABLE IF NOT EXISTS `Paciente-Enfermedad` (
  `idEnfermedad` int NOT NULL,
  `PacienteDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`idEnfermedad`,`PacienteDNI`),
  KEY `fk_Enfermedad_has_Paciente_Paciente1_idx` (`PacienteDNI`),
  KEY `fk_Enfermedad_has_Paciente_Enfermedad1_idx` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_Paciente_Enfermedad1` FOREIGN KEY (`idEnfermedad`) REFERENCES `Enfermedad` (`idEnfermedad`),
  CONSTRAINT `fk_Enfermedad_has_Paciente_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Paciente-Medicamento
DROP TABLE IF EXISTS `Paciente-Medicamento`;
CREATE TABLE IF NOT EXISTS `Paciente-Medicamento` (
  `idMedicamento` int NOT NULL,
  `PacienteDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`idMedicamento`,`PacienteDNI`),
  KEY `fk_Medicamento_has_Paciente_Paciente1_idx` (`PacienteDNI`),
  KEY `fk_Medicamento_has_Paciente_Medicamento1_idx` (`idMedicamento`),
  CONSTRAINT `fk_Medicamento_has_Paciente_Medicamento1` FOREIGN KEY (`idMedicamento`) REFERENCES `Medicamento` (`idMedicamento`),
  CONSTRAINT `fk_Medicamento_has_Paciente_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Paciente-ProductoNatural
DROP TABLE IF EXISTS `Paciente-ProductoNatural`;
CREATE TABLE IF NOT EXISTS `Paciente-ProductoNatural` (
  `idProductoNatural` int NOT NULL,
  `PacienteDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`idProductoNatural`,`PacienteDNI`),
  KEY `fk_ProductoNatural_has_Paciente_Paciente1_idx` (`PacienteDNI`),
  KEY `fk_ProductoNatural_has_Paciente_ProductoNatural1_idx` (`idProductoNatural`),
  CONSTRAINT `fk_ProductoNatural_has_Paciente_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`),
  CONSTRAINT `fk_ProductoNatural_has_Paciente_ProductoNatural1` FOREIGN KEY (`idProductoNatural`) REFERENCES `ProductoNatural` (`idProductoNatural`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.PCR
DROP TABLE IF EXISTS `PCR`;
CREATE TABLE IF NOT EXISTS `PCR` (
  `idPrueba` int NOT NULL,
  `Fluorescencia` double DEFAULT NULL,
  PRIMARY KEY (`idPrueba`),
  KEY `fk_PCR_Prueba1_idx` (`idPrueba`),
  CONSTRAINT `fk_PCR_Prueba1` FOREIGN KEY (`idPrueba`) REFERENCES `Prueba` (`idPrueba`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.ProductoNatural
DROP TABLE IF EXISTS `ProductoNatural`;
CREATE TABLE IF NOT EXISTS `ProductoNatural` (
  `idProductoNatural` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Contraindicaciones` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Posologia` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Prospecto` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Composicion` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`idProductoNatural`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.ProductoNatural-Lenguaje
DROP TABLE IF EXISTS `ProductoNatural-Lenguaje`;
CREATE TABLE IF NOT EXISTS `ProductoNatural-Lenguaje` (
  `idLenguaje` int NOT NULL,
  `idProductoNatural` int NOT NULL,
  PRIMARY KEY (`idLenguaje`,`idProductoNatural`),
  KEY `fk_Lenguaje_has_ProductoNatural_ProductoNatural1_idx` (`idProductoNatural`),
  KEY `fk_Lenguaje_has_ProductoNatural_Lenguaje1_idx` (`idLenguaje`),
  CONSTRAINT `fk_Lenguaje_has_ProductoNatural_Lenguaje1` FOREIGN KEY (`idLenguaje`) REFERENCES `Lenguaje` (`idLenguaje`),
  CONSTRAINT `fk_Lenguaje_has_ProductoNatural_ProductoNatural1` FOREIGN KEY (`idProductoNatural`) REFERENCES `ProductoNatural` (`idProductoNatural`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.ProductoNatural-URL
DROP TABLE IF EXISTS `ProductoNatural-URL`;
CREATE TABLE IF NOT EXISTS `ProductoNatural-URL` (
  `idProductoNatural` int NOT NULL,
  `idURL` int NOT NULL,
  PRIMARY KEY (`idProductoNatural`,`idURL`),
  KEY `fk_ProductoNatural_has_URL_URL1_idx` (`idURL`),
  KEY `fk_ProductoNatural_has_URL_ProductoNatural1_idx` (`idProductoNatural`),
  CONSTRAINT `fk_ProductoNatural_has_URL_ProductoNatural1` FOREIGN KEY (`idProductoNatural`) REFERENCES `ProductoNatural` (`idProductoNatural`),
  CONSTRAINT `fk_ProductoNatural_has_URL_URL1` FOREIGN KEY (`idURL`) REFERENCES `URL` (`idURL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Prueba
DROP TABLE IF EXISTS `Prueba`;
CREATE TABLE IF NOT EXISTS `Prueba` (
  `idPrueba` int NOT NULL AUTO_INCREMENT,
  `TrabajadorLaboratorioDNI` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PacienteDNI` varchar(10) NOT NULL,
  `FechaRealizacion` datetime DEFAULT NULL,
  PRIMARY KEY (`idPrueba`),
  KEY `fk_Prueba_Paciente1_idx` (`PacienteDNI`),
  KEY `fk_Prueba_TrabajadorLaboratorio1_idx` (`TrabajadorLaboratorioDNI`),
  CONSTRAINT `fk_Prueba_Paciente1` FOREIGN KEY (`PacienteDNI`) REFERENCES `Paciente` (`PacienteDNI`),
  CONSTRAINT `fk_Prueba_TrabajadorLaboratorio1` FOREIGN KEY (`TrabajadorLaboratorioDNI`) REFERENCES `TrabajadorLaboratorio` (`TrabajadorLaboratorioDNI`),
  UNIQUE(TrabajadorLaboratorioDNI, PacienteDNI, FechaRealizacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Roles
DROP TABLE IF EXISTS `Roles`;
CREATE TABLE IF NOT EXISTS `Roles` (
  `rol` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_ci NOT NULL,
  PRIMARY KEY (`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Sintomas
DROP TABLE IF EXISTS `Sintomas`;
CREATE TABLE IF NOT EXISTS `Sintomas` (
  `idSintomas` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Descripcion` mediumtext,
  PRIMARY KEY (`idSintomas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Tokenizacion
DROP TABLE IF EXISTS `Tokenizacion`;
CREATE TABLE IF NOT EXISTS `Tokenizacion` (
  `Token` int NOT NULL AUTO_INCREMENT,
  `Usuario_DNI` varchar(10) NOT NULL,
  PRIMARY KEY (`Token`,`Usuario_DNI`),
  KEY `fk_Tokenizacion_Usuario1_idx` (`Usuario_DNI`),
  CONSTRAINT `fk_Tokenizacion_Usuario1` FOREIGN KEY (`Usuario_DNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.TrabajadorLaboratorio
DROP TABLE IF EXISTS `TrabajadorLaboratorio`;
CREATE TABLE IF NOT EXISTS `TrabajadorLaboratorio` (
  `TrabajadorLaboratorioDNI` varchar(10) NOT NULL,
  PRIMARY KEY (`TrabajadorLaboratorioDNI`),
  CONSTRAINT `fk_TrabajadorLaboratorio_Usuario1` FOREIGN KEY (`TrabajadorLaboratorioDNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.URL
DROP TABLE IF EXISTS `URL`;
CREATE TABLE IF NOT EXISTS `URL` (
  `idURL` int NOT NULL AUTO_INCREMENT,
  `URL` tinytext,
  PRIMARY KEY (`idURL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Usuario
DROP TABLE IF EXISTS `Usuario`;
CREATE TABLE IF NOT EXISTS `Usuario` (
  `DNI` varchar(10) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `contrasena` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
  `apellidos` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` DATE DEFAULT NULL,
  `genero` enum('Hombre','Mujer','Otro') DEFAULT NULL,
  `correo_electronico` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla core.Usuario-Impostor
DROP TABLE IF EXISTS `Usuario-Impostor`;
CREATE TABLE IF NOT EXISTS `Usuario-Impostor` (
  `Usuario_DNI` varchar(10) NOT NULL,
  `Impostor_ImpostorDNI` varchar(10) NOT NULL,
  `Token` int NOT NULL,
  PRIMARY KEY (`Usuario_DNI`,`Impostor_ImpostorDNI`),
  KEY `fk_Usuario_has_Impostor_Impostor1_idx` (`Impostor_ImpostorDNI`),
  KEY `fk_Usuario_has_Impostor_Usuario1_idx` (`Usuario_DNI`),
  KEY `FiltradoPorToken` (`Token`),
  CONSTRAINT `fk_Usuario_has_Impostor_Impostor1` FOREIGN KEY (`Impostor_ImpostorDNI`) REFERENCES `Impostor` (`ImpostorDNI`),
  CONSTRAINT `fk_Usuario_has_Impostor_Usuario1` FOREIGN KEY (`Usuario_DNI`) REFERENCES `Usuario` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
