package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.model.montevideo.DataMontevideo;
import app.model.seattle.DataSeattle;
import app.model.tegucigalpa.DataTegucigalpa;
import app.services.ETLService;

import javax.validation.Valid;

@RestController
public class ETLController {
    
    @Autowired
    private ETLService servicioETL ;


    @RequestMapping(method = RequestMethod.POST, value = "/getDataTegucigalpa")
    public ResponseEntity<DataTegucigalpa> addDataTegucigalpa(@RequestBody @Valid DataTegucigalpa data) {

        String str = "key: " + data.getKey() + "\n";
        
        if (data.getEmployees() != null) {
            str = str + "employees: [\n";
            for (int i = 0; i < data.getEmployees().length; i++) {
                str = str + "\t{\n" + "\t\tdni: " + data.getEmployees()[i].getDni() + "\n" + "\t\tnombre: "
                        + data.getEmployees()[i].getNombre() + "\n" + "\t\tapellidos: "
                        + data.getEmployees()[i].getApellidos() + "\n" + "\t\tfechaNacimiento: "
                        + data.getEmployees()[i].getFechaNacimiento() + "\n" + "\t\tcorreoElectronico: "
                        + data.getEmployees()[i].getCorreoElectronico() + "\n" + "\t\tgenero: "
                        + data.getEmployees()[i].getGenero() + "\n" + "\t\tusername: "
                        + data.getEmployees()[i].getUsername() + "\n" + "\t\tpassword: "
                        + data.getEmployees()[i].getPassword() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }

        if (data.getPatients() != null) {
            str = str + "patients: [\n";
            for (int i = 0; i < data.getPatients().length; i++) {
                str = str + "\t{\n" + "\t\tdni: " + data.getPatients()[i].getDni() + "\n" + "\t\tnombre: "
                        + data.getPatients()[i].getNombre() + "\n" + "\t\tapellidos: "
                        + data.getPatients()[i].getApellidos() + "\n" + "\t\tfechaNacimiento: "
                        + data.getPatients()[i].getFechaNacimiento() + "\n" + "\t\tcorreoElectronico: "
                        + data.getPatients()[i].getCorreoElectronico() + "\n" + "\t\tgenero: "
                        + data.getPatients()[i].getGenero() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }

        if (data.getBloodTest() != null) {
            str = str + "bloodTest: [\n";
            for (int i = 0; i < data.getBloodTest().length; i++) {
                str = str + "\t{\n" + "\t\tpatientID: " + data.getBloodTest()[i].getPatientID() + "\n"
                        + "\t\tdoctorID: " + data.getBloodTest()[i].getDoctorID() + "\n" + "\t\telectrolitos: "
                        + data.getBloodTest()[i].getElectrolitos() + "\n" + "\t\tglucosa: "
                        + data.getBloodTest()[i].getGlucosa() + "\n" + "\t\tcolesterol: "
                        + data.getBloodTest()[i].getColesterol() + "\n" + "\t\ttrigliceridos: "
                        + data.getBloodTest()[i].getTrigliceridos() + "\n" + "\t\tbilirrubina: "
                        + data.getBloodTest()[i].getBilirrubina() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }

        if (data.getPcr() != null) {
            str = str + "pcr: [\n";
            for (int i = 0; i < data.getPcr().length; i++) {
                str = str + "\t{\n" + "\t\tpatientID: " + data.getPcr()[i].getPatientID() + "\n" + "\t\tdoctorID: "
                        + data.getPcr()[i].getDoctorID() + "\n" + "\t\tfluorescencia: "
                        + data.getPcr()[i].getFluorescencia() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }
        
        //str = data.toString();
        // System.out.println(str);
        boolean proccessed = servicioETL.proccessTegucigalpa(data);
        System.out.println(proccessed?"DATA HAS BEEN PROCCESSED CORRECTLY":"ERROR ON PROCESSING");

        return ResponseEntity.ok(data);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getDataSeattle")
    public ResponseEntity<DataSeattle> addDataSeattle(@Valid @RequestBody  DataSeattle data) {
        String str;
        str = "key: " + data.getKey() + "\n";
        if (data.getEmployees() != null) {
            str = str + "employees: [\n";
            for (int i = 0; i < data.getEmployees().length; i++) {
                str = str + "\t{\n" + "\t\tdni: " + data.getEmployees()[i].getDni() + "\n" + "\t\tnombre: "
                        + data.getEmployees()[i].getNombre() + "\n" + "\t\tapellidos: "
                        + data.getEmployees()[i].getApellidos() + "\n" + "\t\tfechaNacimiento: "
                        + data.getEmployees()[i].getFechaNacimiento() + "\n" + "\t\tcorreoElectonico: "
                        + data.getEmployees()[i].getCorreoElectronico() + "\n" + "\t\tgenero: "
                        + data.getEmployees()[i].getGenero() + "\n" + "\t\tpassword: "
                        + data.getEmployees()[i].getPassword() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }
        if (data.getPatients() != null) {
            str = str + "patients: [\n";
            for (int i = 0; i < data.getPatients().length; i++) {
                str = str + "\t{\n" + "\t\tdni: " + data.getPatients()[i].getDni() + "\n" + "\t\tnombre: "
                        + data.getPatients()[i].getNombre() + "\n" + "\t\tapellidos: "
                        + data.getPatients()[i].getApellidos() + "\n" + "\t\tfechaNacimiento: "
                        + data.getPatients()[i].getFechaNacimiento() + "\n" + "\t\tcorreoElectonico: "
                        + data.getPatients()[i].getCorreoElectronico() + "\n" + "\t\tgenero: "
                        + data.getPatients()[i].getGenero() + "\n" + "\t\tpassword: "
                        + data.getPatients()[i].getPassword() + "\n" + "\t}\n";
            }
            str = str + "]\n";
        }
        if (data.getConsultas() != null) {
            str = str + "consulta: [\n";
            for (int i = 0; i < data.getConsultas().length; i++) {
                str = str + "\t{\n" + "\t\tidConsulta: " + data.getConsultas()[i].getIdConsulta() + "\n"
                        + "\t\tdniDoctor: " + data.getConsultas()[i].getDniDoctor() + "\n" + "\t\tdniPaciente: "
                        + data.getConsultas()[i].getDniPaciente() + "\n" + "\t\tfechaRegistroPaciente: "
                        + data.getConsultas()[i].getFechaRegistroPaciente() + "\n" + "\t\tfechaConsulta: "
                        + data.getConsultas()[i].getFechaConsulta() + "\n";
                if (data.getConsultas()[i].getEnfermedadesDiagnosticadas() != null) {
                    str = str + "\t\tenfermedadesDiagnosticadas: [\n";
                    for (int j = 0; j < data.getConsultas()[i].getEnfermedadesDiagnosticadas().length; j++) {
                        str = str + "\t\t\t" + data.getConsultas()[i].getEnfermedadesDiagnosticadas()[j] + "\n";
                    }
                    str = str + "\t\t]\n";
                }
                if (data.getConsultas()[i].getMedicamentosRecetados() != null) {
                    str = str + "\t\tmedicamentosRecetados: [\n";
                    for (int j = 0; j < data.getConsultas()[i].getMedicamentosRecetados().length; j++) {
                        str = str + "\t\t\t" + data.getConsultas()[i].getMedicamentosRecetados()[j] + "\n";
                    }
                    str = str + "\t\t]\n";
                }
                if (data.getConsultas()[i].getPruebasLabRealizar() != null) {
                    str = str + "\t\tpruebasLabRealizar: [\n";
                    for (int j = 0; j < data.getConsultas()[i].getPruebasLabRealizar().length; j++) {
                        str = str + "\t\t\t" + data.getConsultas()[i].getPruebasLabRealizar()[j] + "\n";
                    }
                    str = str + "\t\t]\n";
                }
                if (data.getConsultas()[i].getObservaciones() != null) {
                    str = str + "\t\tobservaciones: [\n";
                    for (int j = 0; j < data.getConsultas()[i].getObservaciones().length; j++) {
                        str = str + "\t\t\t" + data.getConsultas()[i].getObservaciones()[j] + "\n";
                    }
                    str = str + "\t\t]\n";
                }
                str = str + "\t\t]\n\t}\n";
            }
            str = str + "]";
        }
        //str = data.toString();
        // System.out.println(str);


        boolean proccessed = servicioETL.proccessSeattle(data);
        System.out.println(proccessed?"DATA HAS BEEN PROCCESSED CORRECTLY":"ERROR ON PROCESSING");
        return ResponseEntity.ok(data);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getDataMontevideo")
    public ResponseEntity<DataMontevideo> addDataMontevideo(@RequestBody @Valid DataMontevideo data) {

        String infor = "key: " + data.getKey() + "\n";

        if (data.getMedicamentos() != null) {
            infor += "medicamentos: [\n\t{\n";
            for (int i = 0; i < data.getMedicamentos().length; i++) {
                infor += "\t\tnombre: " + data.getMedicamentos()[i].getNombre() + ",\n" + "\t\tcontaindicaciones: "
                        + data.getMedicamentos()[i].getContraind() + ",\n" + "\t\tposología: "
                        + data.getMedicamentos()[i].getPosologia() + ",\n" + "\t\tprospecto: "
                        + data.getMedicamentos()[i].getProspecto() + ",\n" + "\t\tcomposición : "
                        + data.getMedicamentos()[i].getComposicion() + ",\n" + "\t\turl : "
                        + data.getMedicamentos()[i].getUrl() + ",\n" + "\t\tidioma : "
                        + data.getMedicamentos()[i].getIdioma() + "\n\t}\n]\n";
            }

        }

        if (data.getEnfermedades() != null) {
            infor += "enfermedades: [\n\t{\n";
            for (int i = 0; i < data.getEnfermedades().length; i++) {
                infor += "\t\tnombre: " + data.getEnfermedades()[i].getNombre() + ",\n" + "\t\tsíntomas: "
                        + data.getEnfermedades()[i].getSintomas() + ",\n" + "\t\trecomendaciones: "
                        + data.getEnfermedades()[i].getRecomendaciones() + ",\n" + "\t\turl : "
                        + data.getEnfermedades()[i].getUrl() + ",\n" + "\t\tidioma : "
                        + data.getEnfermedades()[i].getIdioma() + "\n\t}\n]\n";
            }

        }

        if (data.getProductos_naturales() != null) {
            infor += "medicamentos: [\n\t{\n";
            for (int i = 0; i < data.getProductos_naturales().length; i++) {
                infor += "\t\tnombre: " + data.getProductos_naturales()[i].getNombre() + ",\n"
                        + "\t\tcontaindicaciones: " + data.getProductos_naturales()[i].getContraind() + ",\n"
                        + "\t\tposología: " + data.getProductos_naturales()[i].getPosologia() + ",\n" + "\t\tprospecto: "
                        + data.getProductos_naturales()[i].getProspecto() + ",\n" + "\t\tcomposición : "
                        + data.getProductos_naturales()[i].getComposicion() + ",\n" + "\t\turl : "
                        + data.getProductos_naturales()[i].getUrl() + ",\n" + "\t\tidioma : "
                        + data.getProductos_naturales()[i].getIdioma() + "\n\t}\n]\n";
            }

        }
        //infor = data.toString();
        // System.out.println(infor);

        boolean proccessed = servicioETL.proccessMontevideo(data);
        System.out.println(proccessed?"DATA HAS BEEN PROCCESSED CORRECTLY":"ERROR ON PROCESSING");

        return ResponseEntity.ok(data);
    }
}
