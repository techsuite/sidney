package app.model.seattle;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * Employee Class:
 *  dni : DNI del empleado
 *  nombre : nombre del empleado
 *  apellidos : apellidos del empleado
 *  fechaNacimiento : año-mes-dia
 *  correoElectronico : correo electronico del empleado
 *  genero : genero del empleado
 *  password : password
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Employee {

    private String dni;
    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String correoElectronico;
    private String genero;
    private String password;
    
}