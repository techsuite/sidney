package app.model.seattle;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * Patient Class:
 *  dni : DNI del paciente
 *  nombre : nombre del paciente
 *  apellidos : apellidos del paciente
 *  fechaNacimiento : año-mes-dia
 *  correoElectronico : correo electronico del paciente
 *  genero : genero del paciente
 *  password : password
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Patient {
    
    private String dni;
    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String correoElectronico;
    private String genero;
    private String password;

}
