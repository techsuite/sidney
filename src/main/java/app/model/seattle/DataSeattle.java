package app.model.seattle;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 * Seattle Data Class:
 *  key : password de ciudad
 *  employees : [ información de los empleados ]
 *  patients : [ informacion sobre los pacientes ]
 *  consulta : [ informacion sobre las consultas ]
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DataSeattle {

  @NotNull
  private String key;

  @NotNull
  private Employee[] employees;

  @NotNull
  private Patient[] patients;

  @NotNull
  private Consulta[] consultas;
}
