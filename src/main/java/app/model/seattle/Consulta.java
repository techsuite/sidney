package app.model.seattle;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
 * Consulta Class:
 *  idConsulta : identificador de la consulta
 *  dniDoctor : DNI doctor
 *  nombreDoctor : nombre del doctor
 *  fechaRegistroPaciente : año-mes-dia
 *  fechaConsulta : año-mes-dia
 *  enfermedadesDiagnosticadas : [ informacion sobre enfermedades ]
 *  medicamentosRecetados : [informacion sobre medicamentos ]
 *  pruebasLabRealizar : [ informacion sobre pruebas ]
 *  observaciones : [ observaciones ]
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Consulta {
    
    int idConsulta;
    String dniDoctor;
    String dniPaciente;
    String fechaRegistroPaciente;
    String fechaConsulta;
    String[] enfermedadesDiagnosticadas;
    String[] medicamentosRecetados;
    String[] pruebasLabRealizar;
    String[] observaciones;

}
