package app.model.testPost;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Number Class:
 * senderID : Name of the sender of such number 
 * number: the number sent
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NumberClass {
    private String senderID;
    private Double number;
}
