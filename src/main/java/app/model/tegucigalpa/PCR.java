package app.model.tegucigalpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PCR {
    private String patientID;
    private String doctorID;
    private Double fluorescencia;
    private String fecha;
}