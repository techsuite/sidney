package app.model.tegucigalpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BloodTest {
    private String patientID;
    private String doctorID;
    private Double electrolitos;
    private Double glucosa;
    private Double colesterol;
    private Double trigliceridos;
    private Double bilirrubina;
    private String fecha;
}