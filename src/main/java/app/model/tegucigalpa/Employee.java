package app.model.tegucigalpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Employee {
    private String dni;
    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String correoElectronico;
    private String genero;
    private String username;
    private String password;
}
