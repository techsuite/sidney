package app.model.tegucigalpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DataTegucigalpa {
    @NotNull
    private String key;
    @NotNull
    private Employee employees[];
    @NotNull
    private Patient patients[];
    @NotNull
    private BloodTest bloodTest[];
    @NotNull
    private PCR pcr[];
}
