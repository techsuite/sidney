package app.model.montevideo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Enfermedad {
    private String nombre;
    private String sintomas;
    private String recomendaciones;
    private String url;
    private String idioma;
}
