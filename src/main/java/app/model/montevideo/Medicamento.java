package app.model.montevideo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Medicamento {
    private String nombre;
    private String contraind;
    private String posologia;
    private String prospecto;
    private String composicion;
    private String url;
    private String idioma;

}
