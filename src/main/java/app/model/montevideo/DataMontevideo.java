package app.model.montevideo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class DataMontevideo {
    @NotNull
    private String key;
    private Medicamento medicamentos[];
    private Enfermedad enfermedades[];
    private ProductoNatural productos_naturales[];
}
