package app.services;

import app.dao.MontevideoDAO;
import app.dao.SeattleDAO;
import app.dao.TegucigalpaDAO;
import app.model.montevideo.DataMontevideo;
import app.model.seattle.DataSeattle;
import app.model.tegucigalpa.DataTegucigalpa;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Getter
@Service
public class InsertionService {

  @Autowired
  SeattleDAO seattleDAO;

  @Autowired
  TegucigalpaDAO tegucigalpaDAO;

  @Autowired
  MontevideoDAO montevideoDAO;

  public void insertSeattle(DataSeattle sData) {
    for (int i = 0; i < sData.getPatients().length; i++) {
      seattleDAO.insertPaciente(sData.getPatients()[i]);
    }
    for (int i = 0; i < sData.getEmployees().length; i++) {
      seattleDAO.insertEmpleado(sData.getEmployees()[i]);
    }
    for (int i = 0; i < sData.getConsultas().length; i++) {
      seattleDAO.insertConsulta(sData.getConsultas()[i]);
      seattleDAO.insertConsultaEnfermedad(sData.getConsultas()[i]);
      seattleDAO.insertConsultaMedicamento(sData.getConsultas()[i]);
      seattleDAO.insertConsultaPruebaLab(sData.getConsultas()[i]);
    }
  }

  public void insertTegucigalpa(DataTegucigalpa tData) {
    for (int i = 0; i < tData.getPatients().length; i++) {
      tegucigalpaDAO.insertPaciente(tData.getPatients()[i]);
    }
    for (int i = 0; i < tData.getEmployees().length; i++) {
      tegucigalpaDAO.insertEmpleado(tData.getEmployees()[i]);
    }
    for (int i = 0; i < tData.getBloodTest().length; i++) {
      tegucigalpaDAO.insertAnalisisSangre(tData.getBloodTest()[i]);
    }
    for (int i = 0; i < tData.getPcr().length; i++) {
      tegucigalpaDAO.insertPcr(tData.getPcr()[i]);
    }
  }

  public void insertMontevideo(DataMontevideo mData) {
    for (int i = 0; i < mData.getEnfermedades().length; i++) {
      montevideoDAO.insertEnfermedad(mData.getEnfermedades()[i]);
    }
    for (int i = 0; i < mData.getMedicamentos().length; i++) {
      montevideoDAO.insertMedicamento(mData.getMedicamentos()[i]);
    }
    for (int i = 0; i < mData.getProductos_naturales().length; i++) {
      montevideoDAO.insertProductoNatural(mData.getProductos_naturales()[i]);
    }
  }
}
