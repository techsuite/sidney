package app.services;

import app.model.montevideo.DataMontevideo;
import app.model.seattle.DataSeattle;
import app.model.tegucigalpa.DataTegucigalpa;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Setter
@Getter
@Service
public class ETLService {

  List<DataSeattle> proccessedSeattle;
  List<DataMontevideo> proccessedMontevideo;
  List<DataTegucigalpa> proccessedTegucigalpa;

  boolean debug;

  @Autowired
  InsertionService insertionService;

  public ETLService() {
    proccessedSeattle = new ArrayList<DataSeattle>();
    proccessedMontevideo = new ArrayList<DataMontevideo>();
    proccessedTegucigalpa = new ArrayList<DataTegucigalpa>();
    debug = false;
  }

  public boolean proccessSeattle(DataSeattle data) {
    // Pattern de una fecha
    String regex =
      "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

    /* keyOk (true si la password es correcta)
           arrNotNull (para cada vector es true si mandan datos)
           notInconsist (true si para cada dato no hay inconsistencias)
           res (true si no existen ningún dato con inconsistencias en data)*/
    boolean arrNotNull, notInconsist, res;
    DataSeattle dataRes = new DataSeattle();

    // Comprobamos que la clave es correcta, si no lo es mandamos un error
    if (data.getKey() == null || !data.getKey().equals("W6YRf#*ZLsZ1")) {
      System.out.println("Data Key Incorrect");
      return false;
    }
    // ARRAY DE EMPLEADOS ************************************************************************
    // Array que nos mandan
    app.model.seattle.Employee[] arr = data.getEmployees();
    // Nueva lista con los empleados correctos (será lo que se mande al módulo de inserción)
    List<app.model.seattle.Employee> empleados = new ArrayList<>();
    // Empleado auxiliar. Objeto que se insertará en la lista empleados
    app.model.seattle.Employee empleado;

    arrNotNull = (arr != null);
    res = arrNotNull;

    // Recorremos el array de empleados que nos mandan
    for (int i = 0; arrNotNull && i < arr.length; i++) {
      empleado = new app.model.seattle.Employee();

      // Comprobamos DNI
      notInconsist =
        (
          arr[i].getDni() != null &&
          arr[i].getDni().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setDni(arr[i].getDni()); else {
        res = false;
        System.out.println("Data DNI Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos nombre
      notInconsist =
        (
          arr[i].getNombre() != null &&
          arr[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setNombre(arr[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos apellidos
      notInconsist =
        (
          arr[i].getApellidos() != null &&
          arr[i].getApellidos().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setApellidos(arr[i].getApellidos()); else {
        res = false;
        System.out.println("Data Apellidos Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos fecha de nacimiento
      notInconsist = Pattern.matches(regex, arr[i].getFechaNacimiento());
      if (notInconsist) empleado.setFechaNacimiento(
        arr[i].getFechaNacimiento()
      ); else {
        res = false;
        System.out.println("Data Fecha nacimiento Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos correo
      EmailValidator validatorEmail = EmailValidator.getInstance(false);
      notInconsist = validatorEmail.isValid(arr[i].getCorreoElectronico());
      if (notInconsist) empleado.setCorreoElectronico(
        arr[i].getCorreoElectronico()
      ); else {
        res = false;
        System.out.println("Data Correo Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos password
      notInconsist =
        (
          arr[i].getPassword() != null &&
          arr[i].getPassword().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setPassword(arr[i].getPassword()); else {
        res = false;
        System.out.println("Data Password Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos genero (cambiamos al estándar en el nuevo empleado a insertar)
      notInconsist = arr[i].getGenero() != null;
      if (
        notInconsist && arr[i].getGenero().toLowerCase().equals("masculino")
      ) {
        empleado.setGenero("Hombre");
      } else if (
        notInconsist && arr[i].getGenero().toLowerCase().equals("femenino")
      ) {
        empleado.setGenero("Mujer");
      } else if (notInconsist && arr[i].getGenero().toLowerCase().equals("x")) {
        empleado.setGenero("Otro");
      } else {
        res = false;
        System.out.println(
          "Data Gender Incorrect -isNull or Not in format(masuclino/femenino/x)-Employee"
        );
        continue;
      }

      // Si llega a este punto es que no hay datos inconsistentes luego añadimos al empleado a la lista
      empleados.add(empleado);
    }

    dataRes.setEmployees(empleados.toArray(new app.model.seattle.Employee[0]));

    // ARRAY DE PACIENTES ************************************************************************
    // Array que nos mandan
    app.model.seattle.Patient[] arr2 = data.getPatients();
    // Nueva lista con los pacientes correctos (será lo que se mande al módulo de inserción)
    List<app.model.seattle.Patient> pacientes = new ArrayList<>();
    // Paciente auxiliar. Objeto que se insertará en la lista pacientes
    app.model.seattle.Patient paciente;

    arrNotNull = (arr2 != null);
    res &= arrNotNull;
    // Recorremos el array de pacientes que nos mandan
    for (int i = 0; arrNotNull && i < arr2.length; i++) {
      paciente = new app.model.seattle.Patient();

      // Comprobamos DNI
      notInconsist =
        (
          arr2[i].getDni() != null &&
          arr2[i].getDni().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setDni(arr2[i].getDni()); else {
        res = false;
        System.out.println("Data DNI Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos nombre
      notInconsist =
        (
          arr2[i].getNombre() != null &&
          arr2[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setNombre(arr2[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos apellidos
      notInconsist =
        (
          arr2[i].getApellidos() != null &&
          arr2[i].getApellidos().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setApellidos(arr2[i].getApellidos()); else {
        res = false;
        System.out.println("Data Apellidos Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos fecha de nacimiento
      notInconsist = Pattern.matches(regex, arr2[i].getFechaNacimiento());
      if (notInconsist) paciente.setFechaNacimiento(
        arr2[i].getFechaNacimiento()
      ); else {
        res = false;
        System.out.println("Data Fecha nacimiento Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos correo
      EmailValidator validatorEmail = EmailValidator.getInstance(false);
      notInconsist = validatorEmail.isValid(arr2[i].getCorreoElectronico());
      if (notInconsist) paciente.setCorreoElectronico(
        arr2[i].getCorreoElectronico()
      ); else {
        res = false;
        System.out.println("Data Correo Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos password
      notInconsist =
        (arr2[i].getPassword() != null && arr2[i].getPassword() == "");
      if (notInconsist) paciente.setPassword(arr2[i].getPassword()); else {
        res = false;
        System.out.println("Data Password Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos genero (cambiamos al estándar en el nuevo paciente a insertar)
      notInconsist = arr2[i].getGenero() != null;
      if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("masculino")
      ) {
        paciente.setGenero("Hombre");
      } else if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("femenino")
      ) {
        paciente.setGenero("Mujer");
      } else if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("x")
      ) {
        paciente.setGenero("Otro");
      } else {
        res = false;
        System.out.println(
          "Data Gender Incorrect -isNull or Not in format(masuclino/femenino/x)-Patients"
        );
        continue;
      }

      // Si llega a este punto es que no hay datos inconsistentes luego añadimos al paciente a la lista
      pacientes.add(paciente);
    }

    dataRes.setPatients(pacientes.toArray(new app.model.seattle.Patient[0]));

    // ARRAY DE CONSULTAS ************************************************************************
    // Array que nos mandan
    app.model.seattle.Consulta[] arr3 = data.getConsultas();
    // Nueva lista con las consultas correctas (será lo que se mande al módulo de inserción)
    List<app.model.seattle.Consulta> consultas = new ArrayList<>();
    // Consulta auxiliar. Objeto que se insertará en la lista consultas
    app.model.seattle.Consulta consulta;

    String pcr = "PCR";
    String bloodtest = "AnalisisSangre";
    String tac = "TAC";

    arrNotNull = (arr3 != null);
    res &= arrNotNull;
    // Recorremos el array de consultas que nos mandan
    for (int i = 0; arrNotNull && i < arr3.length; i++) {
      consulta = new app.model.seattle.Consulta();

      // Comprobamos DNI Doctor
      notInconsist =
        (
          arr3[i].getDniDoctor() != null &&
          arr3[i].getDniDoctor().replace(" ", "").length() > 0
        );
      if (notInconsist) consulta.setDniDoctor(arr3[i].getDniDoctor()); else {
        res = false;
        System.out.println("Data DNI_Doctor Incorrect -isEmpty - Consulta");
        continue;
      }
      // Comprobamos DNI Paciente
      notInconsist =
        (
          arr3[i].getDniPaciente() != null &&
            arr3[i].getDniPaciente().replace(" ", "").length() > 0
            ? notInconsist
            : false
        );
      if (notInconsist) consulta.setDniPaciente(
        arr3[i].getDniPaciente()
      ); else {
        res = false;
        System.out.println("Data DNI_Paciente Incorrect -isEmpty - Consulta");
        continue;
      }
      // Comprobamos la fecha_Registro_Paciente
      notInconsist = Pattern.matches(regex, arr3[i].getFechaRegistroPaciente());
      if (notInconsist) consulta.setFechaRegistroPaciente(
        arr3[i].getFechaRegistroPaciente()
      ); else {
        res = false;
        System.out.println(
          "Data Fecha Registro Paciente Incorrect -isEmpty - Consulta"
        );
        continue;
      }
      // Comprobamos la fecha_Consulta
      notInconsist = Pattern.matches(regex, arr3[i].getFechaConsulta());
      if (notInconsist) consulta.setFechaConsulta(
        arr3[i].getFechaConsulta()
      ); else {
        res = false;
        System.out.println("Data Fecha Consulta Incorrect -isEmpty - Consulta");
        continue;
      }
      // Comprobamos las enfermedades diagnosticadas
      notInconsist = (arr3[i].getEnfermedadesDiagnosticadas() != null);
      if (notInconsist) consulta.setEnfermedadesDiagnosticadas(
        arr3[i].getEnfermedadesDiagnosticadas()
      ); else {
        res = false;
        System.out.println(
          "Data Enfermedades Diagnosticadas Incorrect -isEmpty - Consulta"
        );
        continue;
      }
      // Comprobamos medicamentos recetados
      notInconsist = (arr3[i].getMedicamentosRecetados() != null);
      if (notInconsist) consulta.setMedicamentosRecetados(
        arr3[i].getMedicamentosRecetados()
      ); else {
        res = false;
        System.out.println(
          "Data Medicamentos Realizados Incorrect -isEmpty - Consulta"
        );
        continue;
      }
      // Comprobamos las pruebas de laboratorio a realizar
      arrNotNull = (arr3[i].getPruebasLabRealizar() != null);
      List<String> pruebas = new ArrayList<>();
      for (
        int j = 0;
        arrNotNull && j < arr3[i].getPruebasLabRealizar().length;
        j++
      ) {
        switch (arr3[i].getPruebasLabRealizar()[j].toLowerCase()) {
          case "pcr":
            pruebas.add(pcr);
            break;
          case "analisis de sangre":
          case "analisis":
            pruebas.add(bloodtest);
            break;
          case "tac":
            pruebas.add(tac);
            break;
          default:
            notInconsist = false;
            System.out.println(
              "Data Prueba Incorrect - isNot pcr/analisis/tac - Consulta :" +
              arr3[i].getPruebasLabRealizar()
            );
        }
      }
      if (notInconsist) consulta.setPruebasLabRealizar(
        pruebas.toArray(new String[0])
      ); else {
        res = false;
        continue;
      }

      // Comprobamos las observaciones
      notInconsist = (arr3[i].getObservaciones() != null);
      if (notInconsist) consulta.setObservaciones(
        arr3[i].getObservaciones()
      ); else {
        res = false;
        System.out.println("Data Observaciones Incorrect -isEmpty - Consulta");
        continue;
      }

      consultas.add(consulta);
    }
    dataRes.setConsultas(consultas.toArray(new app.model.seattle.Consulta[0]));

    // LLAMAMOS AL MODULO DE INSERCION
    if (debug) proccessedSeattle.add(dataRes); else {
      insertionService.insertSeattle(dataRes);
      System.out.println("Inserción"); //////////// Insertar llamada a inserción cuando nos la den
    }
    return res;
  }

  public boolean proccessMontevideo(DataMontevideo data) {
    /* Mismos atributos que en seattle */
    boolean arrNotNull, notInconsist, res;
    DataMontevideo dataRes = new DataMontevideo();

    // Comprobamos que la clave es correcta, si no lo es mandamos un error
    if (data.getKey() == null || !data.getKey().equals("Fl9z%oJu#pLb")) {
      System.out.println("Data Key Incorrect");
      return false;
    }

    String english = "1";
    String spanish = "27";

    // ARRAY DE MEDICAMENTOS ************************************************************************
    // Array que nos mandan
    app.model.montevideo.Medicamento[] arr = data.getMedicamentos();
    // Nueva lista con los medicamentos correctos (será lo que se mande al módulo de inserción)
    List<app.model.montevideo.Medicamento> medicamentos = new ArrayList<>();
    // Medicamento auxiliar. Objeto que se insertará en la lista medicamentos
    app.model.montevideo.Medicamento medicamento;

    arrNotNull = (arr != null);
    res = arrNotNull;
    // Recorremos el array de medicamentos que nos mandan
    for (int i = 0; arrNotNull && i < arr.length; i++) {
      medicamento = new app.model.montevideo.Medicamento();

      // Comprobamos nombre
      notInconsist =
        (
          arr[i].getNombre() != null &&
          arr[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) medicamento.setNombre(arr[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty - Medicamento");
        continue;
      }
      // Comprobamos contraindicaciones
      notInconsist = arr[i].getContraind() != null;
      if (notInconsist) medicamento.setContraind(arr[i].getContraind()); else {
        res = false;
        System.out.println(
          "Data Contraindicaciones Incorrect -isEmpty - Medicamento"
        );
        continue;
      }
      // Comprobamos posología
      notInconsist =
        (
          arr[i].getPosologia() != null &&
          arr[i].getPosologia().replace(" ", "").length() > 0
        );
      if (notInconsist) medicamento.setPosologia(arr[i].getPosologia()); else {
        res = false;
        System.out.println("Data Posología Incorrect -isEmpty - Medicamento");
        continue;
      }
      // Comprobamos prospecto
      notInconsist =
        (
          arr[i].getProspecto() != null &&
          arr[i].getProspecto().replace(" ", "").length() > 0
        );
      if (notInconsist) medicamento.setProspecto(arr[i].getProspecto()); else {
        res = false;
        System.out.println("Data Prospecto Incorrect -isEmpty - Medicamento");
        continue;
      }
      // Comprobamos composición
      notInconsist =
        (
          arr[i].getComposicion() != null &&
          arr[i].getComposicion().replace(" ", "").length() > 0
        );
      if (notInconsist) medicamento.setComposicion(
        arr[i].getComposicion()
      ); else {
        res = false;
        System.out.println("Data Composicion Incorrect -isEmpty - Medicamento");
        continue;
      }
      // Comprobamos url
      notInconsist =
        (
          arr[i].getUrl() != null &&
          arr[i].getUrl().replace(" ", "").length() > 0
        );
      if (notInconsist) medicamento.setUrl(arr[i].getUrl()); else {
        res = false;
        System.out.println("Data URL Incorrect -isEmpty - Medicamento");
        continue;
      }
      // Comprobamos idioma
      notInconsist = arr[i].getIdioma() != null;
      if (
        notInconsist && (arr[i].getIdioma()).toLowerCase().equals("english")
      ) {
        medicamento.setIdioma(english);
      } else if (
        notInconsist && (arr[i].getIdioma()).toLowerCase().equals("spanish")
      ) {
        medicamento.setIdioma(spanish);
      } else {
        notInconsist = false;
      }
      if (!notInconsist) {
        res = false;
        System.out.println("Data Idioma Incorrect - Medicameto");
        continue;
      }

      medicamentos.add(medicamento);
    }

    dataRes.setMedicamentos(
      medicamentos.toArray(new app.model.montevideo.Medicamento[0])
    );

    // ARRAY DE ENFERMEDADES ************************************************************************
    // Array que nos mandan
    app.model.montevideo.Enfermedad[] arr2 = data.getEnfermedades();
    // Nueva lista con los enfermedades correctos (será lo que se mande al módulo de inserción)
    List<app.model.montevideo.Enfermedad> enfermedades = new ArrayList<>();
    // Enfermedad auxiliar. Objeto que se insertará en la lista enfermedades
    app.model.montevideo.Enfermedad enfermedad;

    arrNotNull = (arr2 != null);
    res &= arrNotNull;
    // Recorremos el array de enfermedades que nos mandan
    for (int i = 0; arrNotNull && i < arr2.length; i++) {
      enfermedad = new app.model.montevideo.Enfermedad();

      // Comprobamos nombre
      notInconsist =
        (
          arr2[i].getNombre() != null &&
          arr2[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) enfermedad.setNombre(arr2[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty - Enfermedad");
        continue;
      }
      // Comprobamos síntomas
      notInconsist =
        (
          arr2[i].getSintomas() != null &&
          arr2[i].getSintomas().replace(" ", "").length() > 0
        );
      if (notInconsist) enfermedad.setSintomas(arr2[i].getSintomas()); else {
        res = false;
        System.out.println("Data Sintomas Incorrect -isEmpty - Enfermedad");
        continue;
      }
      // Comprobamos recomendaciones
      notInconsist =
        (
          arr2[i].getRecomendaciones() != null &&
          arr2[i].getRecomendaciones().replace(" ", "").length() > 0
        );
      if (notInconsist) enfermedad.setRecomendaciones(
        arr2[i].getRecomendaciones()
      ); else {
        res = false;
        System.out.println(
          "Data Recomendaciones Incorrect -isEmpty - Enfermedad"
        );
        continue;
      }
      // Comprobamos url
      notInconsist =
        (
          arr2[i].getUrl() != null &&
          arr2[i].getUrl().replace(" ", "").length() > 0
        );
      if (notInconsist) enfermedad.setUrl(arr2[i].getUrl()); else {
        res = false;
        System.out.println("Data URL Incorrect -isEmpty - Enfermedad");
        continue;
      }
      // Comprobamos idioma
      notInconsist = arr2[i].getIdioma() != null;
      if (
        notInconsist && (arr2[i].getIdioma()).toLowerCase().equals("english")
      ) {
        enfermedad.setIdioma(english);
      } else if (
        notInconsist && (arr2[i].getIdioma()).toLowerCase().equals("spanish")
      ) {
        enfermedad.setIdioma(spanish);
      } else {
        notInconsist = false;
      }
      if (!notInconsist) {
        res = false;
        System.out.println("Data Idioma Incorrect - Enfermedad");
        continue;
      }

      enfermedades.add(enfermedad);
    }

    dataRes.setEnfermedades(
      enfermedades.toArray(new app.model.montevideo.Enfermedad[0])
    );

    // ARRAY DE PRODUCTOS NATURALES ************************************************************************
    // Array que nos mandan
    app.model.montevideo.ProductoNatural[] arr3 = data.getProductos_naturales();
    // Nueva lista con los productos naturales correctos (será lo que se mande al módulo de inserción)
    List<app.model.montevideo.ProductoNatural> productosNat = new ArrayList<>();
    // Producto natural auxiliar. Objeto que se insertará en la lista productos
    app.model.montevideo.ProductoNatural productoNat;

    arrNotNull = (arr3 != null);
    res &= arrNotNull;
    // Recorremos el array de consultas que nos mandan
    for (int i = 0; arrNotNull && i < arr3.length; i++) {
      productoNat = new app.model.montevideo.ProductoNatural();

      // Comprobamos nombre
      notInconsist =
        (
          arr3[i].getNombre() != null &&
          arr3[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) productoNat.setNombre(arr3[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty - ProductoNatural");
        continue;
      }
      // Comprobamos contraindicaciones
      notInconsist = arr3[i].getContraind() != null;
      if (notInconsist) productoNat.setContraind(arr3[i].getContraind()); else {
        res = false;
        System.out.println(
          "Data Contraindicaciones Incorrect -isEmpty - ProductoNatural"
        );
        continue;
      }
      // Comprobamos posología
      notInconsist =
        (
          arr3[i].getPosologia() != null &&
          arr3[i].getPosologia().replace(" ", "").length() > 0
        );
      if (notInconsist) productoNat.setPosologia(arr3[i].getPosologia()); else {
        res = false;
        System.out.println(
          "Data Posología Incorrect -isEmpty - ProductoNatural"
        );
        continue;
      }
      // Comprobamos prospecto
      notInconsist =
        (
          arr3[i].getProspecto() != null &&
          arr3[i].getProspecto().replace(" ", "").length() > 0
        );
      if (notInconsist) productoNat.setProspecto(arr3[i].getProspecto()); else {
        res = false;
        System.out.println(
          "Data Prospecto Incorrect -isEmpty - ProductoNatural"
        );
        continue;
      }
      // Comprobamos composición
      notInconsist =
        (
          arr3[i].getComposicion() != null &&
          arr3[i].getComposicion().replace(" ", "").length() > 0
        );
      if (notInconsist) productoNat.setComposicion(
        arr3[i].getComposicion()
      ); else {
        res = false;
        System.out.println(
          "Data Composicion Incorrect -isEmpty - ProductoNatural"
        );
        continue;
      }
      // Comprobamos url
      notInconsist =
        (
          arr3[i].getUrl() != null &&
          arr3[i].getUrl().replace(" ", "").length() > 0
        );
      if (notInconsist) productoNat.setUrl(arr3[i].getUrl()); else {
        res = false;
        System.out.println("Data URL Incorrect -isEmpty - ProductoNatural");
        continue;
      }
      // Comprobamos idioma
      notInconsist = arr3[i].getIdioma() != null;
      if (
        notInconsist && (arr3[i].getIdioma()).toLowerCase().equals("english")
      ) {
        productoNat.setIdioma(english);
      } else if (
        notInconsist && (arr3[i].getIdioma()).toLowerCase().equals("spanish")
      ) {
        productoNat.setIdioma(spanish);
      } else {
        notInconsist = false;
      }
      if (!notInconsist) {
        res = false;
        System.out.println("Data Idioma Incorrect - ProductoNatural");
        continue;
      }

      productosNat.add(productoNat);
    }

    dataRes.setProductos_naturales(
      productosNat.toArray(new app.model.montevideo.ProductoNatural[0])
    );

    // LLAMAMOS AL MODULO DE INSERCION
    if (debug) proccessedMontevideo.add(dataRes); else {
      insertionService.insertMontevideo(dataRes);
      System.out.println("Inserción"); //////////// Insertar llamada a inserción cuando nos la den
    }
    return res;
  }

  public boolean proccessTegucigalpa(DataTegucigalpa data) {
    // Pattern de una fecha
    String regex =
      "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

    /* Mismos atributos que en Seattle */
    boolean arrNotNull, notInconsist, res;
    DataTegucigalpa dataRes = new DataTegucigalpa();

    // Comprobamos que la clave es correcta, si no lo es mandamos un error
    if (data.getKey() == null || !data.getKey().equals("qnxCE%MIA%m9")) {
      System.out.println("Data Key Incorrect");
      return false;
    }
    // ARRAY DE EMPLEADOS ************************************************************************
    // Array que nos mandan
    app.model.tegucigalpa.Employee[] arr = data.getEmployees();
    // Nueva lista con los empleados correctos (será lo que se mande al módulo de inserción)
    List<app.model.tegucigalpa.Employee> empleados = new ArrayList<>();
    // Empleado auxiliar. Objeto que se insertará en la lista empleados
    app.model.tegucigalpa.Employee empleado;

    arrNotNull = (arr != null);
    res = arrNotNull;
    // Recorremos el array de empleados que nos mandan
    for (int i = 0; arrNotNull && i < arr.length; i++) {
      empleado = new app.model.tegucigalpa.Employee();

      // Comprobamos DNI
      notInconsist =
        (
          arr[i].getDni() != null &&
          arr[i].getDni().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setDni(arr[i].getDni()); else {
        res = false;
        System.out.println("Data DNI Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos nombre
      notInconsist =
        (
          arr[i].getNombre() != null &&
          arr[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setNombre(arr[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos apellidos
      notInconsist =
        (
          arr[i].getApellidos() != null &&
          arr[i].getApellidos().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setApellidos(arr[i].getApellidos()); else {
        res = false;
        System.out.println("Data Apellidos Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos fecha de nacimiento
      notInconsist = Pattern.matches(regex, arr[i].getFechaNacimiento());
      if (notInconsist) empleado.setFechaNacimiento(
        arr[i].getFechaNacimiento()
      ); else {
        res = false;
        System.out.println("Data Fecha nacimiento Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos correo
      EmailValidator validatorEmail = EmailValidator.getInstance(false);
      notInconsist = validatorEmail.isValid(arr[i].getCorreoElectronico());
      if (notInconsist) empleado.setCorreoElectronico(
        arr[i].getCorreoElectronico()
      ); else {
        res = false;
        System.out.println("Data Correo Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos username
      notInconsist =
        (
          arr[i].getUsername() != null &&
          arr[i].getUsername().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setUsername(arr[i].getUsername()); else {
        res = false;
        System.out.println("Data Username Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos password
      notInconsist =
        (
          arr[i].getPassword() != null &&
          arr[i].getPassword().replace(" ", "").length() > 0
        );
      if (notInconsist) empleado.setPassword(arr[i].getPassword()); else {
        res = false;
        System.out.println("Data Password Incorrect -isEmpty-Employee");
        continue;
      }
      // Comprobamos genero (cambiamos al estándar en el nuevo empleado a insertar)
      notInconsist = arr[i].getGenero() != null;
      if (
        notInconsist && arr[i].getGenero().toLowerCase().equals("masculino")
      ) {
        empleado.setGenero("Hombre");
      } else if (
        notInconsist && arr[i].getGenero().toLowerCase().equals("femenino")
      ) {
        empleado.setGenero("Mujer");
      } else if (notInconsist && arr[i].getGenero().toLowerCase().equals("x")) {
        empleado.setGenero("Otro");
      } else {
        res = false;
        System.out.println(
          "Data Gender Incorrect -isNull or Not in format(masuclino/femenino/x)-Employee"
        );
        continue;
      }

      // Si llega a este punto es que no hay datos inconsistentes luego añadimos al empleado a la lista
      empleados.add(empleado);
    }

    dataRes.setEmployees(
      empleados.toArray(new app.model.tegucigalpa.Employee[0])
    );

    // ARRAY DE PACIENTES ************************************************************************
    // Array que nos mandan
    app.model.tegucigalpa.Patient[] arr2 = data.getPatients();
    // Nueva lista con los pacientes correctos (será lo que se mande al módulo de inserción)
    List<app.model.tegucigalpa.Patient> pacientes = new ArrayList<>();
    // Paciente auxiliar. Objeto que se insertará en la lista pacientes
    app.model.tegucigalpa.Patient paciente;

    arrNotNull = (arr2 != null);
    res &= arrNotNull;
    // Recorremos el array de pacientes que nos mandan
    for (int i = 0; arrNotNull && i < arr2.length; i++) {
      paciente = new app.model.tegucigalpa.Patient();

      // Comprobamos DNI
      notInconsist =
        (
          arr2[i].getDni() != null &&
          arr2[i].getDni().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setDni(arr2[i].getDni()); else {
        res = false;
        System.out.println("Data DNI Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos nombre
      notInconsist =
        (
          arr2[i].getNombre() != null &&
          arr2[i].getNombre().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setNombre(arr2[i].getNombre()); else {
        res = false;
        System.out.println("Data Nombre Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos apellidos
      notInconsist =
        (
          arr2[i].getApellidos() != null &&
          arr2[i].getApellidos().replace(" ", "").length() > 0
        );
      if (notInconsist) paciente.setApellidos(arr2[i].getApellidos()); else {
        res = false;
        System.out.println("Data Apellidos Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos fecha de nacimiento
      notInconsist = Pattern.matches(regex, arr2[i].getFechaNacimiento());
      if (notInconsist) paciente.setFechaNacimiento(
        arr2[i].getFechaNacimiento()
      ); else {
        res = false;
        System.out.println("Data Fecha nacimiento Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos correo
      EmailValidator validatorEmail = EmailValidator.getInstance(false);
      notInconsist = validatorEmail.isValid(arr2[i].getCorreoElectronico());
      if (notInconsist) paciente.setCorreoElectronico(
        arr2[i].getCorreoElectronico()
      ); else {
        res = false;
        System.out.println("Data Correo Incorrect -isEmpty-Patients");
        continue;
      }
      // Comprobamos genero (cambiamos al estándar en el nuevo paciente a insertar)
      notInconsist = arr2[i].getGenero() != null;
      if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("masculino")
      ) {
        paciente.setGenero("Hombre");
      } else if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("femenino")
      ) {
        paciente.setGenero("Mujer");
      } else if (
        notInconsist && arr2[i].getGenero().toLowerCase().equals("x")
      ) {
        paciente.setGenero("Otro");
      } else {
        res = false;
        System.out.println(
          "Data Gender Incorrect -isNull or Not in format(masuclino/femenino/x)-Patients"
        );
        continue;
      }

      // Si llega a este punto es que no hay datos inconsistentes luego añadimos al paciente a la lista
      pacientes.add(paciente);
    }

    dataRes.setPatients(
      pacientes.toArray(new app.model.tegucigalpa.Patient[0])
    );

    // ARRAY DE BLOODTESTS ************************************************************************
    // Array que nos mandan
    app.model.tegucigalpa.BloodTest[] arr3 = data.getBloodTest();
    // Nueva lista con los bloodtests correctas (será lo que se mande al módulo de inserción)
    List<app.model.tegucigalpa.BloodTest> bloodTests = new ArrayList<>();
    // Bloodtest auxiliar. Objeto que se insertará en la lista bloodtests
    app.model.tegucigalpa.BloodTest bloodTest;

    arrNotNull = (arr3 != null);
    res &= arrNotNull;
    // Recorremos el array de consultas que nos mandan
    for (int i = 0; arrNotNull && i < arr3.length; i++) {
      bloodTest = new app.model.tegucigalpa.BloodTest();

      // Comprobamos DNI Paciente
      notInconsist =
        (
          arr3[i].getPatientID() != null &&
          arr3[i].getPatientID().replace(" ", "").length() > 0
        );
      if (notInconsist) bloodTest.setPatientID(arr3[i].getPatientID()); else {
        res = false;
        System.out.println("Data DNI_Paciente Incorrect -isEmpty - BloodTest");
        continue;
      }
      // Comprobamos DNI Doctor
      notInconsist =
        (
          arr3[i].getDoctorID() != null &&
          arr3[i].getDoctorID().replace(" ", "").length() > 0
        );
      if (notInconsist) bloodTest.setDoctorID(arr3[i].getDoctorID()); else {
        res = false;
        System.out.println("Data DNI_Doctor Incorrect -isEmpty - BloodTest");
        continue;
      }
      // Comprobar glucosa
      notInconsist = (arr3[i].getGlucosa() >= 0);
      if (notInconsist) bloodTest.setGlucosa(arr3[i].getGlucosa()); else {
        res = false;
        System.out.println(
          "Data nivel_glucosa Incorrect -negative - BloodTest"
        );
        continue;
      }
      // Comprobar colesterol
      notInconsist = (arr3[i].getColesterol() >= 0);
      if (notInconsist) bloodTest.setColesterol(arr3[i].getColesterol()); else {
        res = false;
        System.out.println(
          "Data nivel_colesterol Incorrect -negative - BloodTest"
        );
        continue;
      }
      // Comprobar trigliceridos
      notInconsist = (arr3[i].getTrigliceridos() >= 0);
      if (notInconsist) bloodTest.setTrigliceridos(
        arr3[i].getTrigliceridos()
      ); else {
        res = false;
        System.out.println(
          "Data nivel_trigliceridos Incorrect -negative - BloodTest"
        );
        continue;
      }
      // Comprobar bilirrubina
      notInconsist = (arr3[i].getBilirrubina() >= 0);
      if (notInconsist) bloodTest.setBilirrubina(
        arr3[i].getBilirrubina()
      ); else {
        res = false;
        System.out.println(
          "Data nivel_bilirrubina Incorrect -negative - BloodTest"
        );
        continue;
      }

      bloodTests.add(bloodTest);
    }
    dataRes.setBloodTest(
      bloodTests.toArray(new app.model.tegucigalpa.BloodTest[0])
    );

    // ARRAY DE PCR ************************************************************************
    // Array que nos mandan
    app.model.tegucigalpa.PCR[] arr4 = data.getPcr();
    // Nueva lista con las pcr correctas (será lo que se mande al módulo de inserción)
    List<app.model.tegucigalpa.PCR> pcrs = new ArrayList<>();
    // PCR auxiliar. Objeto que se insertará en la lista pcrs
    app.model.tegucigalpa.PCR pcr;

    arrNotNull = (arr4 != null);
    res &= arrNotNull;
    // Recorremos el array de consultas que nos mandan
    for (int i = 0; arrNotNull && i < arr4.length; i++) {
      pcr = new app.model.tegucigalpa.PCR();

      // Comprobamos DNI Paciente
      notInconsist =
        (
          arr4[i].getPatientID() != null &&
          arr4[i].getPatientID().replace(" ", "").length() > 0
        );
      if (notInconsist) pcr.setPatientID(arr4[i].getPatientID()); else {
        res = false;
        System.out.println("Data DNI_Paciente Incorrect -isEmpty - PCR");
        continue;
      }
      // Comprobamos DNI Doctor
      notInconsist =
        (
          arr4[i].getDoctorID() != null &&
          arr4[i].getDoctorID().replace(" ", "").length() > 0
        );
      if (notInconsist) pcr.setDoctorID(arr4[i].getDoctorID()); else {
        res = false;
        System.out.println("Data DNI_Doctor Incorrect -isEmpty - PCR");
        continue;
      }
      // Comprobar fluorescencia
      notInconsist = (arr4[i].getFluorescencia() >= 0);
      if (notInconsist) pcr.setFluorescencia(arr4[i].getFluorescencia()); else {
        res = false;
        System.out.println(
          "Data nivel_fluorescencia Incorrect -negative - PCR"
        );
        continue;
      }

      // Si llega a este punto es que no hay datos inconsistentes luego añadimos al empleado a la lista
      pcrs.add(pcr);
    }

    dataRes.setPcr(pcrs.toArray(new app.model.tegucigalpa.PCR[0]));

    // we might have to validate this (comment from executives)
    EntryStream
      .of(dataRes.getBloodTest())
      .forKeyValue(
        (index, bt) -> bt.setFecha(data.getBloodTest()[index].getFecha())
      );
    EntryStream
      .of(dataRes.getPcr())
      .forKeyValue((index, bt) -> bt.setFecha(data.getPcr()[index].getFecha()));

    // LLAMAMOS AL MODULO DE INSERCION
    if (debug) proccessedTegucigalpa.add(dataRes); else {
      insertionService.insertTegucigalpa(dataRes);
      System.out.println("Inserción"); //////////// Insertar llamada a inserción cuando nos la den
    }
    return res;
  }
}
