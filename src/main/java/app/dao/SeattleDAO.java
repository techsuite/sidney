package app.dao;

import app.model.seattle.Consulta;
import app.model.seattle.Employee;
import app.model.seattle.Patient;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.stereotype.Repository;

@Repository
public class SeattleDAO extends GenericDAO {

  public void insertEmpleado(Employee empleado) {
    String qUsuario =
      "INSERT INTO `Usuario` (`DNI`,`nombre`,`contrasena`,`apellidos`,`fecha_nacimiento`,`genero`,`correo_electronico`) VALUES ( ? , ? , ? , ? , ? , ? , ? );";
    String qDoctor = "INSERT INTO `Doctor` (`DoctorDNI`) VALUES ( ? );";
    String qToken = "INSERT INTO `Tokenizacion` (`Usuario_DNI`) VALUES ( ? );";

    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(
        conn,
        qUsuario,
        new ScalarHandler<>(),
        empleado.getDni(),
        empleado.getNombre(),
        empleado.getPassword(),
        empleado.getApellidos(),
        empleado.getFechaNacimiento(),
        empleado.getGenero(),
        empleado.getCorreoElectronico()
      );

      queryRunner.insert(
        conn,
        qDoctor,
        new ScalarHandler<>(),
        empleado.getDni()
      );

      queryRunner.insert(
        conn,
        qToken,
        new ScalarHandler<>(),
        empleado.getDni()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertPaciente(Patient paciente) {
    String qUsuario =
      "INSERT INTO `Usuario` (`DNI`,`nombre`,`apellidos`,`fecha_nacimiento`,`genero`,`correo_electronico`) VALUES ( ? , ? , ? , ? , ? , ? );";
    String qPaciente = "INSERT INTO `Paciente` (`PacienteDNI`) VALUES ( ? );";

    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(
        conn,
        qUsuario,
        new ScalarHandler<>(),
        paciente.getDni(),
        paciente.getNombre(),
        paciente.getApellidos(),
        paciente.getFechaNacimiento(),
        paciente.getGenero(),
        paciente.getCorreoElectronico()
      );

      queryRunner.insert(
        conn,
        qPaciente,
        new ScalarHandler<>(),
        paciente.getDni()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertConsulta(Consulta consulta) {
    String qConsulta =
      "INSERT INTO `Consulta` (`DoctorDNI`,`PacienteDNI`,`FechaRealizacion`,`Observaciones`) VALUES ( ? , ? , ? , ? );";

    String observaciones = "";
    for (int i = 0; i < consulta.getObservaciones().length; i++) {
      observaciones += (consulta.getObservaciones()[i] + "\n");
    }

    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(
        conn,
        qConsulta,
        new ScalarHandler<>(),
        consulta.getDniDoctor(),
        consulta.getDniPaciente(),
        consulta.getFechaConsulta(),
        observaciones
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertConsultaEnfermedad(Consulta consulta) {
    String qEnfermedad = "SELECT * FROM `Enfermedad` WHERE Nombre =  ? ;";
    String qPacienteEnfermedad =
      "INSERT INTO `Paciente-Enfermedad` (`idEnfermedad`,`PacienteDNI`) VALUES ( ? , ? );";

    try (Connection conn = connector.getConnection()) {
      Integer idEnfermedad = null;
      for (
        int i = 0;
        i < consulta.getEnfermedadesDiagnosticadas().length;
        i++
      ) {
        idEnfermedad =
          queryRunner.query(
            conn,
            qEnfermedad,
            new ScalarHandler<>(1),
            consulta.getEnfermedadesDiagnosticadas()[i]
          );

        queryRunner.insert(
          conn,
          qPacienteEnfermedad,
          new ScalarHandler<>(),
          idEnfermedad,
          consulta.getDniPaciente()
        );
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertConsultaMedicamento(Consulta consulta) {
    String qMedicamento = "SELECT * FROM `Medicamento` WHERE Nombre =  ? ;";
    String qPacienteMedicamento =
      "INSERT INTO `Paciente-Medicamento` (`idMedicamento`,`PacienteDNI`) VALUES ( ? , ? );";

    try (Connection conn = connector.getConnection()) {
      Integer idMedicamento = null;
      for (int i = 0; i < consulta.getMedicamentosRecetados().length; i++) {
        idMedicamento =
          queryRunner.query(
            conn,
            qMedicamento,
            new ScalarHandler<>(1),
            consulta.getMedicamentosRecetados()[i]
          );

        queryRunner.insert(
          conn,
          qPacienteMedicamento,
          new ScalarHandler<>(),
          idMedicamento,
          consulta.getDniPaciente()
        );
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertConsultaPruebaLab(Consulta consulta) {
    String qPrueba = "INSERT INTO `Prueba` (`PacienteDNI`) VALUES ( ? );";

    try (Connection conn = connector.getConnection()) {
      BigInteger idPrueba = null;
      for (int i = 0; i < consulta.getPruebasLabRealizar().length; i++) {
        idPrueba =
          queryRunner.insert(
            conn,
            qPrueba,
            new ScalarHandler<>(),
            consulta.getDniPaciente()
          );

        String qTipoPrueba =
          "INSERT INTO `" +
          consulta.getPruebasLabRealizar()[i] +
          "` (`idPrueba`) VALUES ( ? );";
        queryRunner.insert(conn, qTipoPrueba, new ScalarHandler<>(), idPrueba);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
