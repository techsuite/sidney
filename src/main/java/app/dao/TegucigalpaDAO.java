package app.dao;

import app.model.tegucigalpa.BloodTest;
import app.model.tegucigalpa.Employee;
import app.model.tegucigalpa.PCR;
import app.model.tegucigalpa.Patient;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.stereotype.Repository;

@Repository
public class TegucigalpaDAO extends GenericDAO {

  public void insertEmpleado(Employee empleado) {
    String qUsuario =
      "INSERT INTO `Usuario` (`DNI`, `nombre`, `contrasena`, `apellidos`, `fecha_nacimiento`, `genero`, `correo_electronico`) VALUES (?, ?, ?, ?, ?, ?, ?);";
    String qTrabajadorLaboratorio =
      "INSERT INTO `TrabajadorLaboratorio` (`TrabajadorLaboratorioDNI`) VALUES (?);";
    String qToken = "INSERT INTO `Tokenizacion` (`Usuario_DNI`) VALUES ( ? );";

    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(
        conn,
        qUsuario,
        new ScalarHandler<>(),
        empleado.getDni(),
        empleado.getNombre(),
        empleado.getPassword(),
        empleado.getApellidos(),
        empleado.getFechaNacimiento(),
        empleado.getGenero(),
        empleado.getCorreoElectronico()
      );
      queryRunner.insert(
        conn,
        qTrabajadorLaboratorio,
        new ScalarHandler<>(),
        empleado.getDni()
      );
      queryRunner.insert(
        conn,
        qToken,
        new ScalarHandler<>(),
        empleado.getDni()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertPaciente(Patient paciente) {
    String qUsuario =
      "INSERT INTO `Usuario` (`DNI`, `nombre`,`apellidos`, `fecha_nacimiento`, `genero`, `correo_electronico`) VALUES (?, ?, ?, ?, ?, ?);";
    String qPaciente = "INSERT INTO `Paciente` (`PacienteDNI`) VALUES(?);";
    try (Connection conn = connector.getConnection()) {
      queryRunner.insert(
        conn,
        qUsuario,
        new ScalarHandler<>(),
        paciente.getDni(),
        paciente.getNombre(),
        paciente.getApellidos(),
        paciente.getFechaNacimiento(),
        paciente.getGenero(),
        paciente.getCorreoElectronico()
      );
      queryRunner.insert(
        conn,
        qPaciente,
        new ScalarHandler<>(),
        paciente.getDni()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertAnalisisSangre(BloodTest analisisSangre) {
    BigInteger generatedId = null;
    String qPrueba =
      "INSERT INTO `Prueba` (`TrabajadorLaboratorioDNI`, `PacienteDNI`, `FechaRealizacion`) VALUES (?, ?,?);";
    String qAnalisisSangre =
      "INSERT INTO `AnalisisSangre` (idPrueba, Electrolitos, Glucosa, Colesterol, Trigliceridos, Bilirrubina) VALUES(?, ?, ?, ?, ?, ?);";
    try (Connection conn = connector.getConnection()) {
      generatedId =
        queryRunner.insert(
          conn,
          qPrueba,
          new ScalarHandler<>(),
          analisisSangre.getDoctorID(),
          analisisSangre.getPatientID(),
          analisisSangre.getFecha()
        );
      queryRunner.insert(
        conn,
        qAnalisisSangre,
        new ScalarHandler<>(),
        generatedId,
        analisisSangre.getElectrolitos(),
        analisisSangre.getGlucosa(),
        analisisSangre.getColesterol(),
        analisisSangre.getTrigliceridos(),
        analisisSangre.getBilirrubina()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertPcr(PCR pcr) {
    BigInteger generatedId = null;
    String qPrueba =
      "INSERT INTO `Prueba` (`TrabajadorLaboratorioDNI`, `PacienteDNI`, `FechaRealizacion`) VALUES (?, ?, ?);";
    String qPcr = "INSERT INTO `PCR` (`idPrueba`, Fluorescencia) VALUES(?, ?);";
    try (Connection conn = connector.getConnection()) {
      generatedId =
        queryRunner.insert(
          conn,
          qPrueba,
          new ScalarHandler<>(),
          pcr.getDoctorID(),
          pcr.getPatientID(),
          pcr.getFecha()
        );
      queryRunner.insert(
        conn,
        qPcr,
        new ScalarHandler<>(),
        generatedId,
        pcr.getFluorescencia()
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
