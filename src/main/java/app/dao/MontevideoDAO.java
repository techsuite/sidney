package app.dao;

import app.model.montevideo.Enfermedad;
import app.model.montevideo.Medicamento;
import app.model.montevideo.ProductoNatural;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.stereotype.Repository;

@Repository
public class MontevideoDAO extends GenericDAO {

  public void insertMedicamento(Medicamento medicamento) {
    String qMedicamento =
      "INSERT INTO `Medicamento` (`Nombre`,`Contraindicaciones`,`Posologia`,`Prospecto`,`Composicion`) VALUES ( ? , ? , ? , ? , ? ) " +
      "ON DUPLICATE KEY UPDATE Nombre=?,Contraindicaciones=?,Posologia=?,Prospecto=?,Composicion=?;";
    String qMedicamentoLeng =
      "INSERT IGNORE INTO `Medicamento-Lenguaje` (`idMedicamento`, `idLenguaje`) VALUES ( ? , ? );";
    String qMedicamentoURL =
      "INSERT IGNORE INTO `Medicamento-URL` (`idMedicamento`, `idURL`) VALUES ( ? , ? );";

    try (Connection conn = connector.getConnection()) {
      BigInteger idMedicamento = null;
      idMedicamento =
        queryRunner.insert(
          conn,
          qMedicamento,
          new ScalarHandler<>(),
          medicamento.getNombre(),
          medicamento.getContraind(),
          medicamento.getPosologia(),
          medicamento.getProspecto(),
          medicamento.getComposicion(),
          medicamento.getNombre(),
          medicamento.getContraind(),
          medicamento.getPosologia(),
          medicamento.getProspecto(),
          medicamento.getComposicion()
        );

      Integer idLenguaje = null;
      BigInteger idURL = null;
      //idLenguaje = buscarLenguaje(medicamento.getIdioma());
      idLenguaje = Integer.parseInt(medicamento.getIdioma());
      idURL = insertarURL(medicamento.getUrl());

      queryRunner.insert(
        conn,
        qMedicamentoLeng,
        new ScalarHandler<>(),
        idMedicamento,
        idLenguaje
      );

      queryRunner.insert(
        conn,
        qMedicamentoURL,
        new ScalarHandler<>(),
        idMedicamento,
        idURL
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertEnfermedad(Enfermedad enfermedad) {
    String qEnfermedad =
      "INSERT INTO `Enfermedad` (`Nombre`,`Sintomas`,`Recomendaciones`) VALUES ( ? , ? , ? ) " +
      "ON DUPLICATE KEY UPDATE Nombre=?, Sintomas=?, Recomendaciones=?";
    String qEnfermedadLeng =
      "INSERT IGNORE INTO `Enfermedad-Lenguaje` (`idEnfermedad`, `idLenguaje`) VALUES ( ? , ? );";
    String qEnfermedadURL =
      "INSERT IGNORE INTO `Enfermedad-URL` (`idEnfermedad`, `idURL`) VALUES ( ? , ? );";

    try (Connection conn = connector.getConnection()) {
      BigInteger idEnfermedad = null;
      idEnfermedad =
        queryRunner.insert(
          conn,
          qEnfermedad,
          new ScalarHandler<>(),
          enfermedad.getNombre(),
          enfermedad.getSintomas(),
          enfermedad.getRecomendaciones(),
          enfermedad.getNombre(),
          enfermedad.getSintomas(),
          enfermedad.getRecomendaciones()
        );

      Integer idLenguaje = null;
      BigInteger idURL = null;
      //idLenguaje = buscarLenguaje(enfermedad.getIdioma());
      idLenguaje = Integer.parseInt(enfermedad.getIdioma());
      idURL = insertarURL(enfermedad.getUrl());

      queryRunner.insert(
        conn,
        qEnfermedadLeng,
        new ScalarHandler<>(),
        idEnfermedad,
        idLenguaje
      );

      queryRunner.insert(
        conn,
        qEnfermedadURL,
        new ScalarHandler<>(),
        idEnfermedad,
        idURL
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertProductoNatural(ProductoNatural productoNatural) {
    String qProductoNatural =
      "INSERT INTO `ProductoNatural` (`Nombre`,`Contraindicaciones`,`Posologia`,`Prospecto`,`Composicion`) VALUES ( ? , ? , ? , ? , ? ) " +
      "ON DUPLICATE KEY UPDATE Nombre=?, Contraindicaciones=?, Posologia=?, Prospecto=?, Composicion=?;";
    String qProductoNaturalLeng =
      "INSERT IGNORE INTO `ProductoNatural-Lenguaje` (`idProductoNatural`, `idLenguaje`) VALUES ( ? , ? );";
    String qProductoNaturalURL =
      "INSERT IGNORE INTO `ProductoNatural-URL` (`idProductoNatural`, `idURL`) VALUES ( ? , ? );";

    try (Connection conn = connector.getConnection()) {
      BigInteger idProductoNatural = null;
      idProductoNatural =
        queryRunner.insert(
          conn,
          qProductoNatural,
          new ScalarHandler<>(),
          productoNatural.getNombre(),
          productoNatural.getContraind(),
          productoNatural.getPosologia(),
          productoNatural.getProspecto(),
          productoNatural.getComposicion(),
          productoNatural.getNombre(),
          productoNatural.getContraind(),
          productoNatural.getPosologia(),
          productoNatural.getProspecto(),
          productoNatural.getComposicion()
        );

      Integer idLenguaje = null;
      BigInteger idURL = null;
      //idLenguaje = buscarLenguaje(productoNatural.getIdioma());
      idLenguaje = Integer.parseInt(productoNatural.getIdioma());
      idURL = insertarURL(productoNatural.getUrl());

      queryRunner.insert(
        conn,
        qProductoNaturalLeng,
        new ScalarHandler<>(),
        idProductoNatural,
        idLenguaje
      );

      queryRunner.insert(
        conn,
        qProductoNaturalURL,
        new ScalarHandler<>(),
        idProductoNatural,
        idURL
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Integer buscarLenguaje(String lenguaje) {
    String qLenguaje = "SELECT * FROM `Lenguaje` WHERE Nombre =  ? ;";
    Integer idLenguaje = null;

    try (Connection conn = connector.getConnection()) {
      idLenguaje =
        queryRunner.query(conn, qLenguaje, new ScalarHandler<>(1), lenguaje);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return idLenguaje;
  }

  public BigInteger insertarURL(String URL) {
    String qURL = "INSERT INTO `URL` (`URL`) VALUES ( ? );";
    BigInteger idURL = null;

    try (Connection conn = connector.getConnection()) {
      idURL = queryRunner.insert(conn, qURL, new ScalarHandler<>(), URL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return idURL;
  }
}
